/*
 * Copyright (C) 2012 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.jm.commons.fio;

import java.io.File;
import java.io.FileFilter;

/**
 * A standard directory file filter
 * 
 * @created Dec 20, 2012
 * @author Michael L.R. Marques
 */
public class DirectoryFileFilter implements FileFilter {
    
    /**
     * Create a static instance
     * 
     * @return 
     */
    public static FileFilter getInstance() {
        return new DirectoryFileFilter();
    }
    
    /**
     * Accept a file or directory
     * 
     * @param pathname
     * @return 
     */
    @Override public boolean accept(File file) {
        return (file.isFile() ||
                    file.isDirectory()) &&
                            file.exists();
    }

}
