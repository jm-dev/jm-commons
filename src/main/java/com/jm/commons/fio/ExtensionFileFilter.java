/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.fio;

import java.io.File;
import java.io.FileFilter;

/**
 * A file filter that filters by extension
 * 
 * @created Dec 19, 2012
 * @author Michael L.R. Marques
 */
public class ExtensionFileFilter implements FileFilter {
    
    /**
     * Collection of extensions
     */
    private String[] extensions;
    
    /**
     * Flag to include directorys
     */
    private boolean includeDirectorys;
    
    /**
     * Create the @code{FileFilter} with collection of extensions
     * 
     * @param extensions 
     */
    public ExtensionFileFilter(String[] extensions) {
        this(extensions, false);
    }
    
    /**
     * Create the @code{FileFilter} with collection of extensions
     * Allow one to include directorys
     * 
     * @param extensions
     * @param includeDirectorys 
     */
    public ExtensionFileFilter(String[] extensions, boolean includeDirectorys) {
        this.extensions = extensions;
        this.includeDirectorys = includeDirectorys;
    }
    
    /**
     * Get the collection of extensions
     * 
     * @return 
     */
    public String[] getExtensions() {
        return this.extensions;
    }
    
    /**
     * Get the flag of include directorys
     * 
     * @return 
     */
    public boolean includeDirectorys() {
        return this.includeDirectorys;
    }
    
    /**
     * Accepts the correct extensions
     * 
     * @param file
     * @return 
     */
    @Override public boolean accept(File file) {
        return this.includeDirectorys &&
                    file.isDirectory() ||
                        FileSystem.isValidExtension(file, this.extensions);
    }

}
