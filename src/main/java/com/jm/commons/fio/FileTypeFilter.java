/*
 * Copyright (C) 2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.jm.commons.fio;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @created Feb 20, 2013
 * @author @author www.codejava.net
 * @reference http://www.codejava.net/java-se/swing/add-file-filter-for-jfilechooser-dialog
 */
public class FileTypeFilter extends FileFilter {
    
    private final String extension;
    private final String description;
    
    /**
     * 
     * @param extension
     * @param description 
     */
    public FileTypeFilter(String extension, String description) {
       this.extension = extension;
       this.description = description;
    }
    
    /**
     * 
     * @param f
     * @return 
     */
    @Override
    public boolean accept(File f) {
        return FileSystem.isValidExtension(f, this.extension);
    }
    
    /**
     * 
     * @return 
     */
    public String getExtension() {
        return this.extension;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String getDescription() {
        return this.description + " (*." + this.extension + ")";
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return getDescription();
    }

}
