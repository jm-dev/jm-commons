/*
 * Copyright (C) 2012 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.models;

import com.jm.commons.fio.DirectoryFileFilter;
import com.jm.commons.fio.FileSystem;
import java.io.File;
import java.io.FileFilter;
import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * A standard @code{DirectoryTreeModel} object
 * 
 * @created Dec 20, 2012
 * @author Michael L.R. Marques
 */
public class DirectoryTreeModel implements TreeModel {
    
    /**
     * The root node as a system file
     */
    private File root;
    
    /**
     * The file filter object
     */
    private FileFilter fileFilter;
    
    /**
     * The event listener list
     */
    private EventListenerList listeneres;
    
    /**
     * Create the tree model using the root file
     * Set the standard directory file filter
     * 
     * @param root 
     */
    public DirectoryTreeModel(File root) {
        this(root, DirectoryFileFilter.getInstance());
    }
    
    /**
     * Create the tree model using the root file
     * Set the file filter
     * 
     * @param root 
     */
    public DirectoryTreeModel(File root, FileFilter fileFilter) {
        this.root = root;
        this.fileFilter = fileFilter;
        this.listeneres = new EventListenerList();
    }
    
    /**
     * Get the file filter
     * 
     * @return 
     */
    public FileFilter getFileFilter() {
        return this.fileFilter;
    }
    
    /**
     * Set the file filter
     * 
     * @param fileFilter 
     */
    public void setFileFilter(FileFilter fileFilter) {
        this.fileFilter = fileFilter;
    }
    
    /**
     * Get the root object
     * 
     * @return 
     */
    @Override public Object getRoot() {
        return this.root;
    }
    
    /**
     * Get the child of the current directory
     * 
     * @param parent
     * @param index
     * @return 
     */
    @Override public Object getChild(Object parent, int index) {
        return FileSystem.listFiles(null, (File) parent, this.fileFilter, false, true).get(index);
    }
    
    /**
     * Get the number of files in the directory
     * 
     * @param parent
     * @return 
     */
    @Override public int getChildCount(Object parent) {
        return FileSystem.listFiles(null, (File) parent, this.fileFilter, false, true).size();
    }
    
    /**
     * Is the node a leaf object
     * 
     * @param node
     * @return 
     */
    @Override public boolean isLeaf(Object node) {
        return ((File) node).isFile();
    }
    
    /**
     * Not needed
     * 
     * @param path
     * @param newValue 
     */
    @Override public void valueForPathChanged(TreePath path, Object newValue) {}
    
    /**
     * Get the index of the child from the parent
     * 
     * @param parent
     * @param child
     * @return 
     */
    @Override public int getIndexOfChild(Object parent, Object child) {
        return FileSystem.listFiles(null, (File) parent, this.fileFilter, false, true).indexOf(child);
    }
    
    /**
     * Add a @code{TreeModelListener}
     * 
     * @param l 
     */
    @Override public void addTreeModelListener(TreeModelListener l) {
        this.listeneres.add(TreeModelListener.class, l);
    }
    
    /**
     * Remove the @code{TreeModelListener}
     * 
     * @param l 
     */
    @Override public void removeTreeModelListener(TreeModelListener l) {
        this.listeneres.remove(TreeModelListener.class, l);
    }

}
