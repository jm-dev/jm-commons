/*
 * Copyright (C) 2012 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.jm.commons.models;

import javax.swing.SpinnerNumberModel;

/**
 * If you want the JSpinner to reset it's value instead of stopping
 * E.g. If the value reaches the minimum value it will reset to the maximum value
 *      and vice versa
 * 
 * @created Dec 20, 2012
 * @author Michael L.R. Marques
 */
public class AdvancedSpinnerNumberModel extends SpinnerNumberModel {
    
    /**
     * Initialize the @code{SpinnerNumberModel}
     * 
     * @param value
     * @param minimum
     * @param maximum
     * @param stepSize 
     */
    public AdvancedSpinnerNumberModel(Number value, Comparable minimum, Comparable maximum, Number stepSize) {
        super(value, minimum, maximum, stepSize);
    }
    
    /**
     * Initialize the @code{SpinnerNumberModel}
     * 
     * @param value
     * @param minimum
     * @param maximum
     * @param stepSize 
     */
    public AdvancedSpinnerNumberModel(int value, int minimum, int maximum, int stepSize) {
        super(value, minimum, maximum, stepSize);
    }
    
    /**
     * Initialize the @code{SpinnerNumberModel}
     * 
     * @param value
     * @param minimum
     * @param maximum
     * @param stepSize 
     */
    public AdvancedSpinnerNumberModel(double value, double minimum, double maximum, double stepSize) {
        super(value, minimum, maximum, stepSize);
    }
    
    /**
     * Initialize the @code{SpinnerNumberModel}
     */
    public AdvancedSpinnerNumberModel() {
        super(Integer.valueOf(0), null, null, Integer.valueOf(1));
    }
    
    /**
     * If the current value is equal to or greater then the maximum value
     * Return the minimum value
     * 
     * @return 
     */
    @Override public Object getNextValue() {
        if (super.getNumber().intValue() >= ((Number) super.getMaximum()).intValue()) {
            return ((Number) super.getMinimum()).intValue();
        }
        return super.getNextValue();
    }


    /**
     * If the current value is equal to or lesser then the minimum value
     * Return the maximum value
     * 
     * @return 
     */
    @Override public Object getPreviousValue() {
        if (super.getNumber().intValue() <= ((Number) super.getMinimum()).intValue()) {
            return ((Number) super.getMaximum()).intValue();
        }
        return super.getPreviousValue();
    }

}
