/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.utils;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;

/**
 * A constants API with common usage values
 * 
 * @author Michael L.R. Marques
 */
public class Constants {
    
    /**
     * Regular expression pattern constant to match a class
     */
    public static final String CLASS_REGEX_PATTERN = "[A-Za-z0-9//^$]+.class";
    
    /**
     * South African long date format constant
     */
    public static final SimpleDateFormat ZA_DATE_LONG = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
    
    /**
     * South African short date format constant
     */
    public static final SimpleDateFormat ZA_DATE_SMALL = new SimpleDateFormat("dd/MM/yyy");
    
    /**
     * South African short time format constant
     */
    public static final SimpleDateFormat ZA_TIME_SMALL = new SimpleDateFormat("HH:mm:ss");

    /**
     * Screen dimension constant (pixels i.e. 1366x768)
     */
    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    
    /**
     * The systems new line constant
     */
    public static final String NEW_LINE = System.getProperty("line.separator");
    
    /**
     * The systems file separator constant
     */
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");
    
    /**
     * The systems path separator constant
     */
    public static final String PATH_SEPARATOR = System.getProperty("path.separator");
    
    /**
     * An empty string constant
     */
    public static final String EMPTY_STRING = "";
    
    /**
     * A space constant
     */
    public static final String SPACE = " ";
    
    /**
     * A period/full-stop constant
     */
    public static final String PERIOD = ".";
    
    /**
     * A tabular constant
     */
    public static final String TAB = "\t";

    /**
     * A positive constant
     */
    public static final int POSITIVE = 1;
    
    /**
     * A negative constant
     */
    public static final int NEGATIVE = -1;
    
    /**
     * A positive sign constant
     */
    public static final String POSITIVE_SIGN = "+";
    
    /**
     * A negative sign constant
     */
    public static final String NEGATIVE_SIGN = "-";
    
}
