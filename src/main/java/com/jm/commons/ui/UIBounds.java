/*
 * Copyright (C) 2012 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.ui;

import com.jm.commons.utils.Constants;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;

/**
 * Resizing and positioning API for containers
 * 
 * @created Dec 19, 2012
 * @author Michael L.R. Marques
 */
public class UIBounds {
    
    /**
     * Returns the bounds of a container centered to the screen
     *
     * @param width
     * @param height
     * @return Rectangle
     */
    public static Rectangle getBounds(int width, int height) {
        return new Rectangle(Constants.SCREEN_SIZE.width / 2 - width / 2, Constants.SCREEN_SIZE.height / 2 - height / 2, width, height);
    }

    /**
     * Returns the bounds of a container centered to the screen
     * 
     * @param size
     * @return 
     */
    public static Rectangle getBounds(Dimension size) {
        return new Rectangle(Constants.SCREEN_SIZE.width / 2 - size.width / 2, Constants.SCREEN_SIZE.height / 2 - size.height / 2, size.width, size.height);
    }

    /**
     * Returns the bounds of a container centered to the screen
     *
     * @param bounds
     * @return Rectangle
     */
    public static Rectangle getBounds(Rectangle bounds) {
        return new Rectangle(Constants.SCREEN_SIZE.width / 2 - bounds.width / 2, Constants.SCREEN_SIZE.height / 2 - bounds.height / 2, bounds.width, bounds.height);
    }
    
    /**
     * Returns the bounds of a container centered to the screen
     *
     * @param container
     * @return Rectangle
     */
    public static Rectangle getBounds(Container container) {
        return new Rectangle(Constants.SCREEN_SIZE.width / 2 - container.getWidth() / 2, Constants.SCREEN_SIZE.height / 2 - container.getHeight() / 2, container.getWidth(), container.getHeight());
    }
    
    /**
     * Sets the bounds of a container centered to the screen
     * 
     * @param container 
     */
    public static void setBounds(Container container) {
        container.setBounds(getBounds(container));
    }

    /**
     * Returns the bounds of a container centered to a percentage of the screen
     *
     * @param percentage
     * @return Rectangle
     */
    public static Rectangle getBounds(double percentage) {
        return getBounds((int) ((percentage / Constants.SCREEN_SIZE.getWidth()) * 100),
                            (int) ((percentage / Constants.SCREEN_SIZE.getHeight()) * 100));
    }
    
    /**
     * Sets the bounds of a container centered to a percentage of the screen
     * 
     * @param container
     * @param percentage 
     */
    public static void setBounds(Container container, double percentage) {
        container.setBounds(getBounds(percentage));
    }

    /**
     * Returns the bounds relative to the center of a parent container
     * 
     * @param parent
     * @param size
     * @return
     */
    public static Rectangle getBounds(Container parent, Dimension size) {
        return new Rectangle((int) (parent.getX() + ((parent.getWidth() / 2) - (size.width / 2))),
                                (int) (parent.getY() + ((parent.getHeight() / 2) - (size.height / 2))),
                                    size.width,
                                        size.height);
    }

    /**
     * Returns the bounds relative to the center of a parent container
     * 
     * @param container
     * @param bounds
     * @return
     */
    public static Rectangle getBounds(Container container, Rectangle bounds) {
        return new Rectangle((int) (container.getX() + ((container.getWidth() / 2) - (bounds.width / 2))),
                                    (int) (container.getY() + ((container.getHeight() / 2) - (bounds.height / 2))),
                                        bounds.width,
                                            bounds.height);
    }
    
    /**
     * Returns the bounds of a child container relative to the center of a parent container
     * 
     * @param parent
     * @param child
     * @return 
     */
    public static Rectangle getBounds(Container parent, Container child) {
        return new Rectangle((int) (parent.getX() + ((parent.getWidth() / 2) - (child.getWidth() / 2))),
                                    (int) (parent.getY() + ((parent.getHeight() / 2) - (child.getHeight() / 2))),
                                        child.getWidth(),
                                            child.getHeight());
    }
    
    /**
     * Sets the bounds of a child container relative to the center of a parent container
     * 
     * @param parent
     * @param child 
     */
    public static void setBounds(Container parent, Container child) {
        child.setBounds(getBounds(parent, child));
    }

}
