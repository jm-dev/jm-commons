/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.commons.xpath;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Michael L.R. Marques
 */
public class XPathNodeList extends XPathNodeListIterator implements List<Node> {
    
    /**
     * 
     * @param xmlDocument
     * @param xPath 
     * @throws XPathExpressionException
     */
    public XPathNodeList(Document xmlDocument, String xPath) throws XPathExpressionException {
        this((NodeList) XPathFactory.newInstance().newXPath().evaluate(xPath, xmlDocument, XPathConstants.NODESET));
    }
    
    /**
     * 
     * @param xmlDocument
     * @param xPath
     * @throws XPathExpressionException 
     */
    public XPathNodeList(File xmlDocument, String xPath) throws XPathExpressionException {
        this((NodeList) XPathFactory.newInstance().newXPath().evaluate(xPath, xmlDocument, XPathConstants.NODESET));
    }
    
    /**
     * 
     * @param xmlDocument
     * @param xPath
     * @throws XPathExpressionException 
     */
    public XPathNodeList(String xmlDocument, String xPath) throws XPathExpressionException {
        this((NodeList) XPathFactory.newInstance().newXPath().evaluate(xPath, new InputSource(xmlDocument), XPathConstants.NODESET));
    }
    
    /**
     * 
     * @param node
     * @param xPath
     * @throws XPathExpressionException 
     */
    public XPathNodeList(Node node, String xPath) throws XPathExpressionException {
        this((NodeList) XPathFactory.newInstance().newXPath().evaluate(xPath, node, XPathConstants.NODESET));
    }
    
    /**
     * 
     * @param xmlNodeList 
     */
    public XPathNodeList(NodeList xmlNodeList) {
        super(xmlNodeList);
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public int size() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(Node e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addAll(Collection<? extends Node> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addAll(int index, Collection<? extends Node> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node get(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node set(int index, Node element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add(int index, Node element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node remove(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator<Node> listIterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator<Node> listIterator(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Node> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
