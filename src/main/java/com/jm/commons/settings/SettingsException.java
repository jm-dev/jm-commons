/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.commons.settings;

/**
 *
 * @author Michael L.R. Marques
 */
public class SettingsException extends Exception {
    
    /**
     * 
     */
    private String name;
    
    /**
     * 
     * @param e 
     */
    public SettingsException(Exception e) {
        this(null, e);
    }
    
    /**
     * 
     * @param message 
     */
    public SettingsException(String message) {
        this(null, message);
    }
    
    /**
     * 
     * @param name
     * @param e 
     */
    public SettingsException(String name, Exception e) {
        super(e);
        this.name = name;
    }
    
    /**
     * 
     * @param name
     * @param message 
     */
    public SettingsException(String name, String message) {
        super(message);
        this.name = name;
    }
    
    /**
     * 
     * @return 
     */
    public String getSettingName() {
        return this.name;
    }
    
}
