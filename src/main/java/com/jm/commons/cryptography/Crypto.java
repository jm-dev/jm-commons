/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.cryptography;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import sun.misc.CharacterDecoder;
import sun.misc.CharacterEncoder;

/**
 * A basic @code{String} encoder/decoder
 * 
 * @created Nov 27, 2012
 * @author Michael L.R. Marques
 */
public class Crypto {
    
    /**
     * The character set that will be used
     */
    private static String characterSet = "UTF-8";
    
    /**
     * The @code{CharacterEncoder} to be used
     */
    private static CharacterEncoder encoder = new BASE64Encoder();
    
    /**
     * The @code{CharacterDecoder} to be used
     */
    private static CharacterDecoder decoder = new BASE64Decoder();
    
    /**
     * Gets the character set property
     * 
     * @return 
     */
    public static String getCharacterSet() {
        return characterSet;
    }
    
    /**
     * Sets the character set property
     * 
     * @param set 
     */
    public static void setCharacterSet(String set) {
        characterSet = set;
    }
    
    /**
     * 
     * @return 
     */
    public static CharacterEncoder getEncoder() {
        return encoder;
    }
    
    /**
     * Sets the @code{CharacterEncoder}
     * 
     * @param enc 
     */
    public static void setEncoder(CharacterEncoder enc) {
        encoder = enc;
    }
    
    /**
     * Gets the @code{CharacterEncoder}
     * 
     * @return 
     */
    public static CharacterDecoder getDecoder() {
        return decoder;
    }
    
    /**
     * Sets the @code{CharacterDecoder}
     * 
     * @param dec 
     */
    public static void setDecoder(CharacterDecoder dec) {
        decoder = dec;
    }
    
    /**
     * Encrypts text
     * 
     * @param text
     * @return 
     */
    public static String encrypt(String text) {
        try {
            return encoder.encode(text.getBytes(characterSet));
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
            return null;
        }
    }
    
    /**
     * Decrypts text
     * 
     * @param text
     * @return 
     */
    public static String decrypt(String text) {
        try {
            return new String(decoder.decodeBuffer(text), characterSet);
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
            return null;
        }
    }

}
