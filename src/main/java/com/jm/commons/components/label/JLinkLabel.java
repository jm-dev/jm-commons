/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.commons.components.label;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import javax.swing.JLabel;

/**
 *
 * @author Michael L.R. Marques
 */
public class JLinkLabel extends JLabel {
    
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    private final transient VetoableChangeSupport vetoableChangeSupport = new java.beans.VetoableChangeSupport(this);
    public static final String PROP_MOUSEOVERFOREGROUND = "PROP_MOUSEOVERFOREGROUND";
    public static final String PROP_MOUSEOVERFONT = "PROP_MOUSEOVERFONT";
    
    private Color mouseOverForeground;
    private Font mouseOverFont;
    
    private Color originalForeground;
    private Font originlFont;
    
    /**
     * 
     */
    public JLinkLabel() {
        super();
        addMouseListener(new MouseOverColourizer());
    }

    /**
     * @return the mouseOverForeground
     */
    public Color getMouseOverForeground() {
        if (mouseOverForeground == null) {
            setMouseOverForeground(getForeground());
        }
        return mouseOverForeground;
    }

    /**
     * @param mouseOverForeground the mouseOverForeground to set
     */
    public void setMouseOverForeground(Color mouseOverForeground) {
        Color oldMouseOverForeground = this.mouseOverForeground;
        try {
            vetoableChangeSupport.fireVetoableChange(PROP_MOUSEOVERFOREGROUND, oldMouseOverForeground, mouseOverForeground);
        } catch (PropertyVetoException pve) {
            pve.printStackTrace();
        }
        this.mouseOverForeground = mouseOverForeground;
        propertyChangeSupport.firePropertyChange(PROP_MOUSEOVERFOREGROUND, oldMouseOverForeground, mouseOverForeground);
    }

    /**
     * @return the mouseOverFont
     */
    public Font getMouseOverFont() {
        if (mouseOverFont == null) {
            setMouseOverFont(getFont());
        }
        return mouseOverFont;
    }

    /**
     * @param mouseOverFont the mouseOverFont to set
     */
    public void setMouseOverFont(Font mouseOverFont) {
        Font oldMouseOverFont = this.mouseOverFont;
        try {
            vetoableChangeSupport.fireVetoableChange(PROP_MOUSEOVERFONT, oldMouseOverFont, mouseOverFont);
        } catch (PropertyVetoException pve) {
            pve.printStackTrace();
        }
        this.mouseOverFont = mouseOverFont;
        propertyChangeSupport.firePropertyChange(PROP_MOUSEOVERFONT, oldMouseOverFont, mouseOverFont);
    }

    /**
     * @return the originalForeground
     */
    private Color getOriginalForeground() {
        return originalForeground;
    }

    /**
     * @param originalForeground the originalForeground to set
     */
    private void setOriginalForeground(Color originalForeground) {
        this.originalForeground = originalForeground;
    }

    /**
     * @return the originlFont
     */
    private Font getOriginlFont() {
        return originlFont;
    }

    /**
     * @param originlFont the originlFont to set
     */
    private void setOriginlFont(Font originlFont) {
        this.originlFont = originlFont;
    }
    
    /**
     * 
     */
    static final class MouseOverColourizer extends MouseAdapter {
        
        /**
         * 
         * @param e 
         */
        @Override
        public void mouseEntered(MouseEvent e) {
            JLinkLabel link = (JLinkLabel) e.getSource();
            link.setOriginalForeground(link.getForeground());
            link.setForeground(link.getMouseOverForeground());
            link.setOriginlFont(link.getFont());
            link.setFont(link.getMouseOverFont());
        }
        
        /**
         * 
         * @param e 
         */
        @Override
        public void mouseExited(MouseEvent e) {
            JLinkLabel link = (JLinkLabel) e.getSource();
            link.setForeground(link.getOriginalForeground());
            link.setFont(link.getOriginlFont());
        }
        
    }
    
}
