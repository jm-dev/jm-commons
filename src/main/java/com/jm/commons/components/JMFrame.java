/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.commons.components;

import com.jm.commons.ui.UIBounds;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JFrame;

/**
 *
 * @author Marquema
 */
public class JMFrame extends JFrame implements ComponentListener {
    
    /**
     * 
     */
    public JMFrame() {
        super();
        addComponentListener(this);
    }
    
    /**
     * 
     * @param parent 
     */
    public JMFrame(Container parent) {
        super();
        addComponentListener(this);
        setBounds(UIBounds.getBounds(parent, this));
    }
    
    /**
     * 
     * @param parent 
     */
    public void setBoundsToCenterOfParent(Container parent) {
        setBounds(UIBounds.getBounds(parent, this));
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void componentResized(ComponentEvent e) {
        Dimension maxSize = getMaximumSize();
        Dimension minSize = getMinimumSize();
        
        int width = (int) Math.ceil(getSize().getWidth());
        if (maxSize.getWidth() > 0 &&
                width > maxSize.getWidth()) {
            width = (int) Math.ceil(maxSize.getWidth());
        } else if (minSize.getWidth() > 0 &&
                width < minSize.getWidth()) {
            width = (int) Math.ceil(minSize.getWidth());
        }
        
        int height = (int) Math.ceil(getSize().getHeight());
        if (maxSize.getHeight() > 0 &&
                height > maxSize.getHeight()) {
            height = (int) Math.ceil(maxSize.getHeight());
        } else if (minSize.getHeight() > 0 &&
                height < minSize.getHeight()) {
            height = (int) Math.ceil(minSize.getHeight());
        }
        
        setSize(width, height);
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void componentMoved(ComponentEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void componentShown(ComponentEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void componentHidden(ComponentEvent e) {
        
    }
    
}
