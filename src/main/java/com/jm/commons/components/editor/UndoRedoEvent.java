/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.editor;

import java.util.EventObject;

/**
 *
 * @created Dec 10, 2012
 * @author Michael L.R. Marques
 */
public class UndoRedoEvent extends EventObject {
    
    /**
     * 
     */
    private boolean undo;
    
    /**
     * 
     */
    private boolean redo;
    
    /**
     * 
     */
    private String oldValue;
    
    /**
     * 
     */
    private String newValue;
    
    /**
     * 
     */
    private UndoRedoEvent.EventType type;
    
    /**
     * 
     * @param source 
     */
    public UndoRedoEvent(Object source, boolean undo, boolean redo, String oldValue, String newValue, UndoRedoEvent.EventType type) {
        super(source);
        this.undo = undo;
        this.redo = redo;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.type = type;
    }
    
    /**
     * 
     * @return 
     */
    public boolean getUndo() {
        return this.undo;
    }
    
    /**
     * 
     * @return 
     */
    public boolean getRedo() {
        return this.redo;
    }
    
    /**
     * 
     * @return 
     */
    public String getOldValue() {
        return this.oldValue;
    }
    
    /**
     * 
     * @return 
     */
    public String getNewValue() {
        return this.newValue;
    }
    
    /**
     * 
     * @return 
     */
    public UndoRedoEvent.EventType getEventType() {
        return this.type;
    }
    
    /**
     * Defines the event types
     */
    public static final class EventType {
        
        /**
         * 
         */
        private String typeString;
        
        /**
         * 
         * @param s 
         */
        private EventType(String s) {
            this.typeString = s;
        }

        /**
         * Opening type.
         */
        public static final UndoRedoEvent.EventType UNDO = new UndoRedoEvent.EventType("UNDO");

        /**
         * Opened type.
         */
        public static final UndoRedoEvent.EventType REDO = new UndoRedoEvent.EventType("REDO");

        /**
         * Forward type.
         */
        public static final UndoRedoEvent.EventType CHANGE = new UndoRedoEvent.EventType("CHANGE");

        /**
         * Converts the type to a string.
         *
         * @return the string
         */
        @Override public String toString() {
            return this.typeString;
        }
        
    }

}
