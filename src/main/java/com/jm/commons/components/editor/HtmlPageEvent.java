/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.editor;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.EventObject;

/**
 *
 * @created Dec 3, 2012
 * @author Michael L.R. Marques
 */
public class HtmlPageEvent extends EventObject {
    
    /**
     * 
     */
    private URL url;
    
    /**
     * 
     */
    private HtmlPageEvent.EventType type;
    
    /**
     * 
     * @param source
     * @param type
     * @param url 
     */
    public HtmlPageEvent(Object source, File file, HtmlPageEvent.EventType type) {
        super(source);
        try {
            this.url = file.toURI().toURL();
        } catch (MalformedURLException murle) {
            murle.printStackTrace();
        }
        this.type = type;
    }
    
    /**
     * 
     * @param source
     * @param type
     * @param url
     * @param desc
     * @param sourceElement
     */
    public HtmlPageEvent(Object source, String url, HtmlPageEvent.EventType type) {
        super(source);
        try {
            this.url = new URI(url).toURL();
        } catch (URISyntaxException | MalformedURLException e) {
            e.printStackTrace();
        }
        this.type = type;
    }
    
    /**
     * 
     * @param source
     * @param type
     * @param url 
     */
    public HtmlPageEvent(Object source, URL url, HtmlPageEvent.EventType type) {
        super(source);
        this.url = url;
        this.type = type;
    }
    
    

    /**
     * Gets the type of event.
     *
     * @return the type
     */
    public HtmlPageEvent.EventType getEventType() {
        return this.type;
    }

    /**
     * Gets the URL that the link refers to.
     *
     * @return the URL
     */
    public URL getURL() {
        return this.url;
    }

    /**
     * Defines the event types
     */
    public static final class EventType {
        
        /**
         * 
         */
        private String typeString;
        
        /**
         * 
         * @param s 
         */
        private EventType(String s) {
            this.typeString = s;
        }

        /**
         * Opening type.
         */
        public static final HtmlPageEvent.EventType OPENING = new HtmlPageEvent.EventType("OPENING");

        /**
         * Opened type.
         */
        public static final HtmlPageEvent.EventType OPENED = new HtmlPageEvent.EventType("OPENED");

        /**
         * Forward type.
         */
        public static final HtmlPageEvent.EventType FORWARD = new HtmlPageEvent.EventType("FORWARD");
        
        /**
         * Back Type.
         */
        public static final HtmlPageEvent.EventType BACK = new HtmlPageEvent.EventType("BACK");

        /**
         * Converts the type to a string.
         *
         * @return the string
         */
        @Override public String toString() {
            return this.typeString;
        }
        
    }

}
