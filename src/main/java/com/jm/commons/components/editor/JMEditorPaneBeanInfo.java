/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.editor;

import java.beans.*;

/**
 *
 * @author Michael L.R. Marques
 */
public class JMEditorPaneBeanInfo extends SimpleBeanInfo {

    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( JMEditorPaneBeanInfo.class , null ); // NOI18N//GEN-HEADEREND:BeanDescriptor

        // Here you can add code for customizing the BeanDescriptor.

        return beanDescriptor;     }//GEN-LAST:BeanDescriptor
    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_actions = 2;
    private static final int PROPERTY_alignmentX = 3;
    private static final int PROPERTY_alignmentY = 4;
    private static final int PROPERTY_ancestorListeners = 5;
    private static final int PROPERTY_autoscrolls = 6;
    private static final int PROPERTY_background = 7;
    private static final int PROPERTY_backgroundSet = 8;
    private static final int PROPERTY_baselineResizeBehavior = 9;
    private static final int PROPERTY_border = 10;
    private static final int PROPERTY_bounds = 11;
    private static final int PROPERTY_caret = 12;
    private static final int PROPERTY_caretColor = 13;
    private static final int PROPERTY_caretListeners = 14;
    private static final int PROPERTY_caretPosition = 15;
    private static final int PROPERTY_colorModel = 16;
    private static final int PROPERTY_component = 17;
    private static final int PROPERTY_componentCount = 18;
    private static final int PROPERTY_componentListeners = 19;
    private static final int PROPERTY_componentOrientation = 20;
    private static final int PROPERTY_componentPopupMenu = 21;
    private static final int PROPERTY_components = 22;
    private static final int PROPERTY_containerListeners = 23;
    private static final int PROPERTY_contentType = 24;
    private static final int PROPERTY_cursor = 25;
    private static final int PROPERTY_cursorSet = 26;
    private static final int PROPERTY_debugGraphicsOptions = 27;
    private static final int PROPERTY_disabledTextColor = 28;
    private static final int PROPERTY_displayable = 29;
    private static final int PROPERTY_document = 30;
    private static final int PROPERTY_doubleBuffered = 31;
    private static final int PROPERTY_dragEnabled = 32;
    private static final int PROPERTY_dropLocation = 33;
    private static final int PROPERTY_dropMode = 34;
    private static final int PROPERTY_dropTarget = 35;
    private static final int PROPERTY_editable = 36;
    private static final int PROPERTY_editorKit = 37;
    private static final int PROPERTY_enabled = 38;
    private static final int PROPERTY_focusable = 39;
    private static final int PROPERTY_focusAccelerator = 40;
    private static final int PROPERTY_focusCycleRoot = 41;
    private static final int PROPERTY_focusCycleRootAncestor = 42;
    private static final int PROPERTY_focusListeners = 43;
    private static final int PROPERTY_focusOwner = 44;
    private static final int PROPERTY_focusTraversable = 45;
    private static final int PROPERTY_focusTraversalKeys = 46;
    private static final int PROPERTY_focusTraversalKeysEnabled = 47;
    private static final int PROPERTY_focusTraversalPolicy = 48;
    private static final int PROPERTY_focusTraversalPolicyProvider = 49;
    private static final int PROPERTY_focusTraversalPolicySet = 50;
    private static final int PROPERTY_font = 51;
    private static final int PROPERTY_fontSet = 52;
    private static final int PROPERTY_foreground = 53;
    private static final int PROPERTY_foregroundSet = 54;
    private static final int PROPERTY_graphics = 55;
    private static final int PROPERTY_graphicsConfiguration = 56;
    private static final int PROPERTY_height = 57;
    private static final int PROPERTY_hierarchyBoundsListeners = 58;
    private static final int PROPERTY_hierarchyListeners = 59;
    private static final int PROPERTY_highlighter = 60;
    private static final int PROPERTY_hyperlinkListeners = 61;
    private static final int PROPERTY_ignoreRepaint = 62;
    private static final int PROPERTY_inheritsPopupMenu = 63;
    private static final int PROPERTY_inputContext = 64;
    private static final int PROPERTY_inputMap = 65;
    private static final int PROPERTY_inputMethodListeners = 66;
    private static final int PROPERTY_inputMethodRequests = 67;
    private static final int PROPERTY_inputVerifier = 68;
    private static final int PROPERTY_insets = 69;
    private static final int PROPERTY_keyListeners = 70;
    private static final int PROPERTY_keymap = 71;
    private static final int PROPERTY_layout = 72;
    private static final int PROPERTY_lightweight = 73;
    private static final int PROPERTY_locale = 74;
    private static final int PROPERTY_location = 75;
    private static final int PROPERTY_locationOnScreen = 76;
    private static final int PROPERTY_managingFocus = 77;
    private static final int PROPERTY_margin = 78;
    private static final int PROPERTY_maximumSize = 79;
    private static final int PROPERTY_maximumSizeSet = 80;
    private static final int PROPERTY_minimumSize = 81;
    private static final int PROPERTY_minimumSizeSet = 82;
    private static final int PROPERTY_mouseListeners = 83;
    private static final int PROPERTY_mouseMotionListeners = 84;
    private static final int PROPERTY_mousePosition = 85;
    private static final int PROPERTY_mouseWheelListeners = 86;
    private static final int PROPERTY_name = 87;
    private static final int PROPERTY_navigationFilter = 88;
    private static final int PROPERTY_nextFocusableComponent = 89;
    private static final int PROPERTY_opaque = 90;
    private static final int PROPERTY_optimizedDrawingEnabled = 91;
    private static final int PROPERTY_paintingForPrint = 92;
    private static final int PROPERTY_paintingTile = 93;
    private static final int PROPERTY_parent = 94;
    private static final int PROPERTY_peer = 95;
    private static final int PROPERTY_preferredScrollableViewportSize = 96;
    private static final int PROPERTY_preferredSize = 97;
    private static final int PROPERTY_preferredSizeSet = 98;
    private static final int PROPERTY_propertyChangeListeners = 99;
    private static final int PROPERTY_registeredKeyStrokes = 100;
    private static final int PROPERTY_requestFocusEnabled = 101;
    private static final int PROPERTY_rootPane = 102;
    private static final int PROPERTY_scrollableTracksViewportHeight = 103;
    private static final int PROPERTY_scrollableTracksViewportWidth = 104;
    private static final int PROPERTY_selectedText = 105;
    private static final int PROPERTY_selectedTextColor = 106;
    private static final int PROPERTY_selectionColor = 107;
    private static final int PROPERTY_selectionEnd = 108;
    private static final int PROPERTY_selectionStart = 109;
    private static final int PROPERTY_showing = 110;
    private static final int PROPERTY_size = 111;
    private static final int PROPERTY_text = 112;
    private static final int PROPERTY_toolkit = 113;
    private static final int PROPERTY_toolTipText = 114;
    private static final int PROPERTY_topLevelAncestor = 115;
    private static final int PROPERTY_transferHandler = 116;
    private static final int PROPERTY_treeLock = 117;
    private static final int PROPERTY_UI = 118;
    private static final int PROPERTY_UIClassID = 119;
    private static final int PROPERTY_valid = 120;
    private static final int PROPERTY_validateRoot = 121;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 122;
    private static final int PROPERTY_vetoableChangeListeners = 123;
    private static final int PROPERTY_visible = 124;
    private static final int PROPERTY_visibleRect = 125;
    private static final int PROPERTY_width = 126;
    private static final int PROPERTY_x = 127;
    private static final int PROPERTY_y = 128;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[129];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", JMEditorPaneBeanInfo.class, "getAccessibleContext", null ); // NOI18N
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", JMEditorPaneBeanInfo.class, "getActionMap", "setActionMap" ); // NOI18N
            properties[PROPERTY_actions] = new PropertyDescriptor ( "actions", JMEditorPaneBeanInfo.class, "getActions", null ); // NOI18N
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", JMEditorPaneBeanInfo.class, "getAlignmentX", "setAlignmentX" ); // NOI18N
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", JMEditorPaneBeanInfo.class, "getAlignmentY", "setAlignmentY" ); // NOI18N
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", JMEditorPaneBeanInfo.class, "getAncestorListeners", null ); // NOI18N
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", JMEditorPaneBeanInfo.class, "getAutoscrolls", "setAutoscrolls" ); // NOI18N
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", JMEditorPaneBeanInfo.class, "getBackground", "setBackground" ); // NOI18N
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", JMEditorPaneBeanInfo.class, "isBackgroundSet", null ); // NOI18N
            properties[PROPERTY_baselineResizeBehavior] = new PropertyDescriptor ( "baselineResizeBehavior", JMEditorPaneBeanInfo.class, "getBaselineResizeBehavior", null ); // NOI18N
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", JMEditorPaneBeanInfo.class, "getBorder", "setBorder" ); // NOI18N
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", JMEditorPaneBeanInfo.class, "getBounds", "setBounds" ); // NOI18N
            properties[PROPERTY_caret] = new PropertyDescriptor ( "caret", JMEditorPaneBeanInfo.class, "getCaret", "setCaret" ); // NOI18N
            properties[PROPERTY_caretColor] = new PropertyDescriptor ( "caretColor", JMEditorPaneBeanInfo.class, "getCaretColor", "setCaretColor" ); // NOI18N
            properties[PROPERTY_caretListeners] = new PropertyDescriptor ( "caretListeners", JMEditorPaneBeanInfo.class, "getCaretListeners", null ); // NOI18N
            properties[PROPERTY_caretPosition] = new PropertyDescriptor ( "caretPosition", JMEditorPaneBeanInfo.class, "getCaretPosition", "setCaretPosition" ); // NOI18N
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", JMEditorPaneBeanInfo.class, "getColorModel", null ); // NOI18N
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", JMEditorPaneBeanInfo.class, null, null, "getComponent", null ); // NOI18N
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", JMEditorPaneBeanInfo.class, "getComponentCount", null ); // NOI18N
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", JMEditorPaneBeanInfo.class, "getComponentListeners", null ); // NOI18N
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", JMEditorPaneBeanInfo.class, "getComponentOrientation", "setComponentOrientation" ); // NOI18N
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", JMEditorPaneBeanInfo.class, "getComponentPopupMenu", "setComponentPopupMenu" ); // NOI18N
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", JMEditorPaneBeanInfo.class, "getComponents", null ); // NOI18N
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", JMEditorPaneBeanInfo.class, "getContainerListeners", null ); // NOI18N
            properties[PROPERTY_contentType] = new PropertyDescriptor ( "contentType", JMEditorPaneBeanInfo.class, "getContentType", "setContentType" ); // NOI18N
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", JMEditorPaneBeanInfo.class, "getCursor", "setCursor" ); // NOI18N
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", JMEditorPaneBeanInfo.class, "isCursorSet", null ); // NOI18N
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", JMEditorPaneBeanInfo.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" ); // NOI18N
            properties[PROPERTY_disabledTextColor] = new PropertyDescriptor ( "disabledTextColor", JMEditorPaneBeanInfo.class, "getDisabledTextColor", "setDisabledTextColor" ); // NOI18N
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", JMEditorPaneBeanInfo.class, "isDisplayable", null ); // NOI18N
            properties[PROPERTY_document] = new PropertyDescriptor ( "document", JMEditorPaneBeanInfo.class, "getDocument", "setDocument" ); // NOI18N
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", JMEditorPaneBeanInfo.class, "isDoubleBuffered", "setDoubleBuffered" ); // NOI18N
            properties[PROPERTY_dragEnabled] = new PropertyDescriptor ( "dragEnabled", JMEditorPaneBeanInfo.class, "getDragEnabled", "setDragEnabled" ); // NOI18N
            properties[PROPERTY_dropLocation] = new PropertyDescriptor ( "dropLocation", JMEditorPaneBeanInfo.class, "getDropLocation", null ); // NOI18N
            properties[PROPERTY_dropMode] = new PropertyDescriptor ( "dropMode", JMEditorPaneBeanInfo.class, "getDropMode", "setDropMode" ); // NOI18N
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", JMEditorPaneBeanInfo.class, "getDropTarget", "setDropTarget" ); // NOI18N
            properties[PROPERTY_editable] = new PropertyDescriptor ( "editable", JMEditorPaneBeanInfo.class, "isEditable", "setEditable" ); // NOI18N
            properties[PROPERTY_editorKit] = new PropertyDescriptor ( "editorKit", JMEditorPaneBeanInfo.class, "getEditorKit", "setEditorKit" ); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", JMEditorPaneBeanInfo.class, "isEnabled", "setEnabled" ); // NOI18N
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", JMEditorPaneBeanInfo.class, "isFocusable", "setFocusable" ); // NOI18N
            properties[PROPERTY_focusAccelerator] = new PropertyDescriptor ( "focusAccelerator", JMEditorPaneBeanInfo.class, "getFocusAccelerator", "setFocusAccelerator" ); // NOI18N
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", JMEditorPaneBeanInfo.class, "isFocusCycleRoot", "setFocusCycleRoot" ); // NOI18N
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", JMEditorPaneBeanInfo.class, "getFocusCycleRootAncestor", null ); // NOI18N
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", JMEditorPaneBeanInfo.class, "getFocusListeners", null ); // NOI18N
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", JMEditorPaneBeanInfo.class, "isFocusOwner", null ); // NOI18N
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", JMEditorPaneBeanInfo.class, "isFocusTraversable", null ); // NOI18N
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", JMEditorPaneBeanInfo.class, null, null, null, "setFocusTraversalKeys" ); // NOI18N
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", JMEditorPaneBeanInfo.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", JMEditorPaneBeanInfo.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", JMEditorPaneBeanInfo.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", JMEditorPaneBeanInfo.class, "isFocusTraversalPolicySet", null ); // NOI18N
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", JMEditorPaneBeanInfo.class, "getFont", "setFont" ); // NOI18N
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", JMEditorPaneBeanInfo.class, "isFontSet", null ); // NOI18N
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", JMEditorPaneBeanInfo.class, "getForeground", "setForeground" ); // NOI18N
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", JMEditorPaneBeanInfo.class, "isForegroundSet", null ); // NOI18N
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", JMEditorPaneBeanInfo.class, "getGraphics", null ); // NOI18N
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", JMEditorPaneBeanInfo.class, "getGraphicsConfiguration", null ); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", JMEditorPaneBeanInfo.class, "getHeight", null ); // NOI18N
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", JMEditorPaneBeanInfo.class, "getHierarchyBoundsListeners", null ); // NOI18N
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", JMEditorPaneBeanInfo.class, "getHierarchyListeners", null ); // NOI18N
            properties[PROPERTY_highlighter] = new PropertyDescriptor ( "highlighter", JMEditorPaneBeanInfo.class, "getHighlighter", "setHighlighter" ); // NOI18N
            properties[PROPERTY_hyperlinkListeners] = new PropertyDescriptor ( "hyperlinkListeners", JMEditorPaneBeanInfo.class, "getHyperlinkListeners", null ); // NOI18N
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", JMEditorPaneBeanInfo.class, "getIgnoreRepaint", "setIgnoreRepaint" ); // NOI18N
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", JMEditorPaneBeanInfo.class, "getInheritsPopupMenu", "setInheritsPopupMenu" ); // NOI18N
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", JMEditorPaneBeanInfo.class, "getInputContext", null ); // NOI18N
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", JMEditorPaneBeanInfo.class, "getInputMap", null ); // NOI18N
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", JMEditorPaneBeanInfo.class, "getInputMethodListeners", null ); // NOI18N
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", JMEditorPaneBeanInfo.class, "getInputMethodRequests", null ); // NOI18N
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", JMEditorPaneBeanInfo.class, "getInputVerifier", "setInputVerifier" ); // NOI18N
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", JMEditorPaneBeanInfo.class, "getInsets", null ); // NOI18N
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", JMEditorPaneBeanInfo.class, "getKeyListeners", null ); // NOI18N
            properties[PROPERTY_keymap] = new PropertyDescriptor ( "keymap", JMEditorPaneBeanInfo.class, "getKeymap", "setKeymap" ); // NOI18N
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", JMEditorPaneBeanInfo.class, "getLayout", "setLayout" ); // NOI18N
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", JMEditorPaneBeanInfo.class, "isLightweight", null ); // NOI18N
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", JMEditorPaneBeanInfo.class, "getLocale", "setLocale" ); // NOI18N
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", JMEditorPaneBeanInfo.class, "getLocation", "setLocation" ); // NOI18N
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", JMEditorPaneBeanInfo.class, "getLocationOnScreen", null ); // NOI18N
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", JMEditorPaneBeanInfo.class, "isManagingFocus", null ); // NOI18N
            properties[PROPERTY_margin] = new PropertyDescriptor ( "margin", JMEditorPaneBeanInfo.class, "getMargin", "setMargin" ); // NOI18N
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", JMEditorPaneBeanInfo.class, "getMaximumSize", "setMaximumSize" ); // NOI18N
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", JMEditorPaneBeanInfo.class, "isMaximumSizeSet", null ); // NOI18N
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", JMEditorPaneBeanInfo.class, "getMinimumSize", "setMinimumSize" ); // NOI18N
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", JMEditorPaneBeanInfo.class, "isMinimumSizeSet", null ); // NOI18N
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", JMEditorPaneBeanInfo.class, "getMouseListeners", null ); // NOI18N
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", JMEditorPaneBeanInfo.class, "getMouseMotionListeners", null ); // NOI18N
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", JMEditorPaneBeanInfo.class, "getMousePosition", null ); // NOI18N
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", JMEditorPaneBeanInfo.class, "getMouseWheelListeners", null ); // NOI18N
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", JMEditorPaneBeanInfo.class, "getName", "setName" ); // NOI18N
            properties[PROPERTY_navigationFilter] = new PropertyDescriptor ( "navigationFilter", JMEditorPaneBeanInfo.class, "getNavigationFilter", "setNavigationFilter" ); // NOI18N
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", JMEditorPaneBeanInfo.class, "getNextFocusableComponent", "setNextFocusableComponent" ); // NOI18N
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", JMEditorPaneBeanInfo.class, "isOpaque", "setOpaque" ); // NOI18N
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", JMEditorPaneBeanInfo.class, "isOptimizedDrawingEnabled", null ); // NOI18N
            properties[PROPERTY_paintingForPrint] = new PropertyDescriptor ( "paintingForPrint", JMEditorPaneBeanInfo.class, "isPaintingForPrint", null ); // NOI18N
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", JMEditorPaneBeanInfo.class, "isPaintingTile", null ); // NOI18N
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", JMEditorPaneBeanInfo.class, "getParent", null ); // NOI18N
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", JMEditorPaneBeanInfo.class, "getPeer", null ); // NOI18N
            properties[PROPERTY_preferredScrollableViewportSize] = new PropertyDescriptor ( "preferredScrollableViewportSize", JMEditorPaneBeanInfo.class, "getPreferredScrollableViewportSize", null ); // NOI18N
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", JMEditorPaneBeanInfo.class, "getPreferredSize", "setPreferredSize" ); // NOI18N
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", JMEditorPaneBeanInfo.class, "isPreferredSizeSet", null ); // NOI18N
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", JMEditorPaneBeanInfo.class, "getPropertyChangeListeners", null ); // NOI18N
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", JMEditorPaneBeanInfo.class, "getRegisteredKeyStrokes", null ); // NOI18N
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", JMEditorPaneBeanInfo.class, "isRequestFocusEnabled", "setRequestFocusEnabled" ); // NOI18N
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", JMEditorPaneBeanInfo.class, "getRootPane", null ); // NOI18N
            properties[PROPERTY_scrollableTracksViewportHeight] = new PropertyDescriptor ( "scrollableTracksViewportHeight", JMEditorPaneBeanInfo.class, "getScrollableTracksViewportHeight", null ); // NOI18N
            properties[PROPERTY_scrollableTracksViewportWidth] = new PropertyDescriptor ( "scrollableTracksViewportWidth", JMEditorPaneBeanInfo.class, "getScrollableTracksViewportWidth", null ); // NOI18N
            properties[PROPERTY_selectedText] = new PropertyDescriptor ( "selectedText", JMEditorPaneBeanInfo.class, "getSelectedText", null ); // NOI18N
            properties[PROPERTY_selectedTextColor] = new PropertyDescriptor ( "selectedTextColor", JMEditorPaneBeanInfo.class, "getSelectedTextColor", "setSelectedTextColor" ); // NOI18N
            properties[PROPERTY_selectionColor] = new PropertyDescriptor ( "selectionColor", JMEditorPaneBeanInfo.class, "getSelectionColor", "setSelectionColor" ); // NOI18N
            properties[PROPERTY_selectionEnd] = new PropertyDescriptor ( "selectionEnd", JMEditorPaneBeanInfo.class, "getSelectionEnd", "setSelectionEnd" ); // NOI18N
            properties[PROPERTY_selectionStart] = new PropertyDescriptor ( "selectionStart", JMEditorPaneBeanInfo.class, "getSelectionStart", "setSelectionStart" ); // NOI18N
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", JMEditorPaneBeanInfo.class, "isShowing", null ); // NOI18N
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", JMEditorPaneBeanInfo.class, "getSize", "setSize" ); // NOI18N
            properties[PROPERTY_text] = new PropertyDescriptor ( "text", JMEditorPaneBeanInfo.class, "getText", "setText" ); // NOI18N
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", JMEditorPaneBeanInfo.class, "getToolkit", null ); // NOI18N
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", JMEditorPaneBeanInfo.class, "getToolTipText", "setToolTipText" ); // NOI18N
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", JMEditorPaneBeanInfo.class, "getTopLevelAncestor", null ); // NOI18N
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", JMEditorPaneBeanInfo.class, "getTransferHandler", "setTransferHandler" ); // NOI18N
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", JMEditorPaneBeanInfo.class, "getTreeLock", null ); // NOI18N
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", JMEditorPaneBeanInfo.class, "getUI", "setUI" ); // NOI18N
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", JMEditorPaneBeanInfo.class, "getUIClassID", null ); // NOI18N
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", JMEditorPaneBeanInfo.class, "isValid", null ); // NOI18N
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", JMEditorPaneBeanInfo.class, "isValidateRoot", null ); // NOI18N
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", JMEditorPaneBeanInfo.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" ); // NOI18N
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", JMEditorPaneBeanInfo.class, "getVetoableChangeListeners", null ); // NOI18N
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", JMEditorPaneBeanInfo.class, "isVisible", "setVisible" ); // NOI18N
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", JMEditorPaneBeanInfo.class, "getVisibleRect", null ); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", JMEditorPaneBeanInfo.class, "getWidth", null ); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", JMEditorPaneBeanInfo.class, "getX", null ); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", JMEditorPaneBeanInfo.class, "getY", null ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties

        // Here you can add code for customizing the properties array.

        return properties;     }//GEN-LAST:Properties
    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_caretListener = 1;
    private static final int EVENT_componentListener = 2;
    private static final int EVENT_containerListener = 3;
    private static final int EVENT_focusListener = 4;
    private static final int EVENT_hierarchyBoundsListener = 5;
    private static final int EVENT_hierarchyListener = 6;
    private static final int EVENT_hyperlinkListener = 7;
    private static final int EVENT_inputMethodListener = 8;
    private static final int EVENT_keyListener = 9;
    private static final int EVENT_mouseListener = 10;
    private static final int EVENT_mouseMotionListener = 11;
    private static final int EVENT_mouseWheelListener = 12;
    private static final int EVENT_propertyChangeListener = 13;
    private static final int EVENT_vetoableChangeListener = 14;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[15];
    
        try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorRemoved", "ancestorMoved"}, "addAncestorListener", "removeAncestorListener" ); // NOI18N
            eventSets[EVENT_caretListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "caretListener", javax.swing.event.CaretListener.class, new String[] {"caretUpdate"}, "addCaretListener", "removeCaretListener" ); // NOI18N
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentMoved", "componentShown", "componentHidden"}, "addComponentListener", "removeComponentListener" ); // NOI18N
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" ); // NOI18N
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" ); // NOI18N
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" ); // NOI18N
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" ); // NOI18N
            eventSets[EVENT_hyperlinkListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "hyperlinkListener", javax.swing.event.HyperlinkListener.class, new String[] {"hyperlinkUpdate"}, "addHyperlinkListener", "removeHyperlinkListener" ); // NOI18N
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"inputMethodTextChanged", "caretPositionChanged"}, "addInputMethodListener", "removeInputMethodListener" ); // NOI18N
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyTyped", "keyPressed", "keyReleased"}, "addKeyListener", "removeKeyListener" ); // NOI18N
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mousePressed", "mouseReleased", "mouseEntered", "mouseExited"}, "addMouseListener", "removeMouseListener" ); // NOI18N
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" ); // NOI18N
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" ); // NOI18N
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" ); // NOI18N
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( JMEditorPaneBeanInfo.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events

        // Here you can add code for customizing the event sets array.

        return eventSets;     }//GEN-LAST:Events
    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_add2 = 2;
    private static final int METHOD_add3 = 3;
    private static final int METHOD_add4 = 4;
    private static final int METHOD_add5 = 5;
    private static final int METHOD_add6 = 6;
    private static final int METHOD_addHtmlViewerListner7 = 7;
    private static final int METHOD_addKeymap8 = 8;
    private static final int METHOD_addNotify9 = 9;
    private static final int METHOD_addPropertyChangeListener10 = 10;
    private static final int METHOD_applyComponentOrientation11 = 11;
    private static final int METHOD_areFocusTraversalKeysSet12 = 12;
    private static final int METHOD_back13 = 13;
    private static final int METHOD_bounds14 = 14;
    private static final int METHOD_canGoBack15 = 15;
    private static final int METHOD_canGoForward16 = 16;
    private static final int METHOD_checkImage17 = 17;
    private static final int METHOD_checkImage18 = 18;
    private static final int METHOD_computeVisibleRect19 = 19;
    private static final int METHOD_contains20 = 20;
    private static final int METHOD_contains21 = 21;
    private static final int METHOD_copy22 = 22;
    private static final int METHOD_countComponents23 = 23;
    private static final int METHOD_createEditorKitForContentType24 = 24;
    private static final int METHOD_createImage25 = 25;
    private static final int METHOD_createImage26 = 26;
    private static final int METHOD_createToolTip27 = 27;
    private static final int METHOD_createVolatileImage28 = 28;
    private static final int METHOD_createVolatileImage29 = 29;
    private static final int METHOD_cut30 = 30;
    private static final int METHOD_deliverEvent31 = 31;
    private static final int METHOD_disable32 = 32;
    private static final int METHOD_dispatchEvent33 = 33;
    private static final int METHOD_doLayout34 = 34;
    private static final int METHOD_enable35 = 35;
    private static final int METHOD_enable36 = 36;
    private static final int METHOD_enableInputMethods37 = 37;
    private static final int METHOD_findComponentAt38 = 38;
    private static final int METHOD_findComponentAt39 = 39;
    private static final int METHOD_fireHyperlinkUpdate40 = 40;
    private static final int METHOD_firePageBack41 = 41;
    private static final int METHOD_firePageForward42 = 42;
    private static final int METHOD_firePageOpened43 = 43;
    private static final int METHOD_firePageOpening44 = 44;
    private static final int METHOD_firePropertyChange45 = 45;
    private static final int METHOD_firePropertyChange46 = 46;
    private static final int METHOD_firePropertyChange47 = 47;
    private static final int METHOD_firePropertyChange48 = 48;
    private static final int METHOD_firePropertyChange49 = 49;
    private static final int METHOD_firePropertyChange50 = 50;
    private static final int METHOD_firePropertyChange51 = 51;
    private static final int METHOD_firePropertyChange52 = 52;
    private static final int METHOD_forward53 = 53;
    private static final int METHOD_getActionForKeyStroke54 = 54;
    private static final int METHOD_getBaseline55 = 55;
    private static final int METHOD_getBounds56 = 56;
    private static final int METHOD_getClientProperty57 = 57;
    private static final int METHOD_getComponentAt58 = 58;
    private static final int METHOD_getComponentAt59 = 59;
    private static final int METHOD_getComponentZOrder60 = 60;
    private static final int METHOD_getConditionForKeyStroke61 = 61;
    private static final int METHOD_getDefaultLocale62 = 62;
    private static final int METHOD_getEditorKitClassNameForContentType63 = 63;
    private static final int METHOD_getEditorKitForContentType64 = 64;
    private static final int METHOD_getFocusTraversalKeys65 = 65;
    private static final int METHOD_getFontMetrics66 = 66;
    private static final int METHOD_getInsets67 = 67;
    private static final int METHOD_getKeymap68 = 68;
    private static final int METHOD_getListeners69 = 69;
    private static final int METHOD_getLocation70 = 70;
    private static final int METHOD_getMousePosition71 = 71;
    private static final int METHOD_getPage72 = 72;
    private static final int METHOD_getPopupLocation73 = 73;
    private static final int METHOD_getPrintable74 = 74;
    private static final int METHOD_getPropertyChangeListeners75 = 75;
    private static final int METHOD_getScrollableBlockIncrement76 = 76;
    private static final int METHOD_getScrollableUnitIncrement77 = 77;
    private static final int METHOD_getSize78 = 78;
    private static final int METHOD_getText79 = 79;
    private static final int METHOD_getToolTipLocation80 = 80;
    private static final int METHOD_getToolTipText81 = 81;
    private static final int METHOD_gotFocus82 = 82;
    private static final int METHOD_grabFocus83 = 83;
    private static final int METHOD_handleEvent84 = 84;
    private static final int METHOD_hasFocus85 = 85;
    private static final int METHOD_hide86 = 86;
    private static final int METHOD_imageUpdate87 = 87;
    private static final int METHOD_insets88 = 88;
    private static final int METHOD_inside89 = 89;
    private static final int METHOD_invalidate90 = 90;
    private static final int METHOD_isAncestorOf91 = 91;
    private static final int METHOD_isFocusCycleRoot92 = 92;
    private static final int METHOD_isLightweightComponent93 = 93;
    private static final int METHOD_keyDown94 = 94;
    private static final int METHOD_keyUp95 = 95;
    private static final int METHOD_layout96 = 96;
    private static final int METHOD_list97 = 97;
    private static final int METHOD_list98 = 98;
    private static final int METHOD_list99 = 99;
    private static final int METHOD_list100 = 100;
    private static final int METHOD_list101 = 101;
    private static final int METHOD_loadKeymap102 = 102;
    private static final int METHOD_locate103 = 103;
    private static final int METHOD_location104 = 104;
    private static final int METHOD_lostFocus105 = 105;
    private static final int METHOD_minimumSize106 = 106;
    private static final int METHOD_modelToView107 = 107;
    private static final int METHOD_mouseDown108 = 108;
    private static final int METHOD_mouseDrag109 = 109;
    private static final int METHOD_mouseEnter110 = 110;
    private static final int METHOD_mouseExit111 = 111;
    private static final int METHOD_mouseMove112 = 112;
    private static final int METHOD_mouseUp113 = 113;
    private static final int METHOD_move114 = 114;
    private static final int METHOD_moveCaretPosition115 = 115;
    private static final int METHOD_nextFocus116 = 116;
    private static final int METHOD_paint117 = 117;
    private static final int METHOD_paintAll118 = 118;
    private static final int METHOD_paintComponents119 = 119;
    private static final int METHOD_paintImmediately120 = 120;
    private static final int METHOD_paintImmediately121 = 121;
    private static final int METHOD_paste122 = 122;
    private static final int METHOD_postEvent123 = 123;
    private static final int METHOD_preferredSize124 = 124;
    private static final int METHOD_prepareImage125 = 125;
    private static final int METHOD_prepareImage126 = 126;
    private static final int METHOD_print127 = 127;
    private static final int METHOD_print128 = 128;
    private static final int METHOD_print129 = 129;
    private static final int METHOD_print130 = 130;
    private static final int METHOD_printAll131 = 131;
    private static final int METHOD_printComponents132 = 132;
    private static final int METHOD_putClientProperty133 = 133;
    private static final int METHOD_read134 = 134;
    private static final int METHOD_read135 = 135;
    private static final int METHOD_registerEditorKitForContentType136 = 136;
    private static final int METHOD_registerEditorKitForContentType137 = 137;
    private static final int METHOD_registerKeyboardAction138 = 138;
    private static final int METHOD_registerKeyboardAction139 = 139;
    private static final int METHOD_remove140 = 140;
    private static final int METHOD_remove141 = 141;
    private static final int METHOD_remove142 = 142;
    private static final int METHOD_removeAll143 = 143;
    private static final int METHOD_removeHtmlViewerListner144 = 144;
    private static final int METHOD_removeKeymap145 = 145;
    private static final int METHOD_removeNotify146 = 146;
    private static final int METHOD_removePropertyChangeListener147 = 147;
    private static final int METHOD_repaint148 = 148;
    private static final int METHOD_repaint149 = 149;
    private static final int METHOD_repaint150 = 150;
    private static final int METHOD_repaint151 = 151;
    private static final int METHOD_repaint152 = 152;
    private static final int METHOD_replaceSelection153 = 153;
    private static final int METHOD_requestDefaultFocus154 = 154;
    private static final int METHOD_requestFocus155 = 155;
    private static final int METHOD_requestFocus156 = 156;
    private static final int METHOD_requestFocusInWindow157 = 157;
    private static final int METHOD_resetKeyboardActions158 = 158;
    private static final int METHOD_reshape159 = 159;
    private static final int METHOD_resize160 = 160;
    private static final int METHOD_resize161 = 161;
    private static final int METHOD_revalidate162 = 162;
    private static final int METHOD_scrollRectToVisible163 = 163;
    private static final int METHOD_scrollToReference164 = 164;
    private static final int METHOD_select165 = 165;
    private static final int METHOD_selectAll166 = 166;
    private static final int METHOD_setBounds167 = 167;
    private static final int METHOD_setComponentZOrder168 = 168;
    private static final int METHOD_setDefaultLocale169 = 169;
    private static final int METHOD_setEditorKitForContentType170 = 170;
    private static final int METHOD_setPage171 = 171;
    private static final int METHOD_setPage172 = 172;
    private static final int METHOD_show173 = 173;
    private static final int METHOD_show174 = 174;
    private static final int METHOD_size175 = 175;
    private static final int METHOD_toString176 = 176;
    private static final int METHOD_transferFocus177 = 177;
    private static final int METHOD_transferFocusBackward178 = 178;
    private static final int METHOD_transferFocusDownCycle179 = 179;
    private static final int METHOD_transferFocusUpCycle180 = 180;
    private static final int METHOD_unregisterKeyboardAction181 = 181;
    private static final int METHOD_update182 = 182;
    private static final int METHOD_updateUI183 = 183;
    private static final int METHOD_validate184 = 184;
    private static final int METHOD_viewToModel185 = 185;
    private static final int METHOD_write186 = 186;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[187];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor(java.awt.Component.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor(java.awt.Component.class.getMethod("add", new Class[] {java.awt.PopupMenu.class})); // NOI18N
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_add2] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_add2].setDisplayName ( "" );
            methods[METHOD_add3] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class})); // NOI18N
            methods[METHOD_add3].setDisplayName ( "" );
            methods[METHOD_add4] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.awt.Component.class, int.class})); // NOI18N
            methods[METHOD_add4].setDisplayName ( "" );
            methods[METHOD_add5] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_add5].setDisplayName ( "" );
            methods[METHOD_add6] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, int.class})); // NOI18N
            methods[METHOD_add6].setDisplayName ( "" );
            methods[METHOD_addHtmlViewerListner7] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("addHtmlViewerListner", new Class[] {com.jm.commons.components.editor.HtmlPageListener.class})); // NOI18N
            methods[METHOD_addHtmlViewerListner7].setDisplayName ( "" );
            methods[METHOD_addKeymap8] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("addKeymap", new Class[] {java.lang.String.class, javax.swing.text.Keymap.class})); // NOI18N
            methods[METHOD_addKeymap8].setDisplayName ( "" );
            methods[METHOD_addNotify9] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("addNotify", new Class[] {})); // NOI18N
            methods[METHOD_addNotify9].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener10] = new MethodDescriptor(java.awt.Container.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_addPropertyChangeListener10].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation11] = new MethodDescriptor(java.awt.Container.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class})); // NOI18N
            methods[METHOD_applyComponentOrientation11].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet12] = new MethodDescriptor(java.awt.Container.class.getMethod("areFocusTraversalKeysSet", new Class[] {int.class})); // NOI18N
            methods[METHOD_areFocusTraversalKeysSet12].setDisplayName ( "" );
            methods[METHOD_back13] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("back", new Class[] {})); // NOI18N
            methods[METHOD_back13].setDisplayName ( "" );
            methods[METHOD_bounds14] = new MethodDescriptor(java.awt.Component.class.getMethod("bounds", new Class[] {})); // NOI18N
            methods[METHOD_bounds14].setDisplayName ( "" );
            methods[METHOD_canGoBack15] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("canGoBack", new Class[] {})); // NOI18N
            methods[METHOD_canGoBack15].setDisplayName ( "" );
            methods[METHOD_canGoForward16] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("canGoForward", new Class[] {})); // NOI18N
            methods[METHOD_canGoForward16].setDisplayName ( "" );
            methods[METHOD_checkImage17] = new MethodDescriptor(java.awt.Component.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage17].setDisplayName ( "" );
            methods[METHOD_checkImage18] = new MethodDescriptor(java.awt.Component.class.getMethod("checkImage", new Class[] {java.awt.Image.class, int.class, int.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage18].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect19] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_computeVisibleRect19].setDisplayName ( "" );
            methods[METHOD_contains20] = new MethodDescriptor(java.awt.Component.class.getMethod("contains", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_contains20].setDisplayName ( "" );
            methods[METHOD_contains21] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("contains", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_contains21].setDisplayName ( "" );
            methods[METHOD_copy22] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("copy", new Class[] {})); // NOI18N
            methods[METHOD_copy22].setDisplayName ( "" );
            methods[METHOD_countComponents23] = new MethodDescriptor(java.awt.Container.class.getMethod("countComponents", new Class[] {})); // NOI18N
            methods[METHOD_countComponents23].setDisplayName ( "" );
            methods[METHOD_createEditorKitForContentType24] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("createEditorKitForContentType", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_createEditorKitForContentType24].setDisplayName ( "" );
            methods[METHOD_createImage25] = new MethodDescriptor(java.awt.Component.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class})); // NOI18N
            methods[METHOD_createImage25].setDisplayName ( "" );
            methods[METHOD_createImage26] = new MethodDescriptor(java.awt.Component.class.getMethod("createImage", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_createImage26].setDisplayName ( "" );
            methods[METHOD_createToolTip27] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("createToolTip", new Class[] {})); // NOI18N
            methods[METHOD_createToolTip27].setDisplayName ( "" );
            methods[METHOD_createVolatileImage28] = new MethodDescriptor(java.awt.Component.class.getMethod("createVolatileImage", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_createVolatileImage28].setDisplayName ( "" );
            methods[METHOD_createVolatileImage29] = new MethodDescriptor(java.awt.Component.class.getMethod("createVolatileImage", new Class[] {int.class, int.class, java.awt.ImageCapabilities.class})); // NOI18N
            methods[METHOD_createVolatileImage29].setDisplayName ( "" );
            methods[METHOD_cut30] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("cut", new Class[] {})); // NOI18N
            methods[METHOD_cut30].setDisplayName ( "" );
            methods[METHOD_deliverEvent31] = new MethodDescriptor(java.awt.Container.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_deliverEvent31].setDisplayName ( "" );
            methods[METHOD_disable32] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("disable", new Class[] {})); // NOI18N
            methods[METHOD_disable32].setDisplayName ( "" );
            methods[METHOD_dispatchEvent33] = new MethodDescriptor(java.awt.Component.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class})); // NOI18N
            methods[METHOD_dispatchEvent33].setDisplayName ( "" );
            methods[METHOD_doLayout34] = new MethodDescriptor(java.awt.Container.class.getMethod("doLayout", new Class[] {})); // NOI18N
            methods[METHOD_doLayout34].setDisplayName ( "" );
            methods[METHOD_enable35] = new MethodDescriptor(java.awt.Component.class.getMethod("enable", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_enable35].setDisplayName ( "" );
            methods[METHOD_enable36] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("enable", new Class[] {})); // NOI18N
            methods[METHOD_enable36].setDisplayName ( "" );
            methods[METHOD_enableInputMethods37] = new MethodDescriptor(java.awt.Component.class.getMethod("enableInputMethods", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_enableInputMethods37].setDisplayName ( "" );
            methods[METHOD_findComponentAt38] = new MethodDescriptor(java.awt.Container.class.getMethod("findComponentAt", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_findComponentAt38].setDisplayName ( "" );
            methods[METHOD_findComponentAt39] = new MethodDescriptor(java.awt.Container.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_findComponentAt39].setDisplayName ( "" );
            methods[METHOD_fireHyperlinkUpdate40] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("fireHyperlinkUpdate", new Class[] {javax.swing.event.HyperlinkEvent.class})); // NOI18N
            methods[METHOD_fireHyperlinkUpdate40].setDisplayName ( "" );
            methods[METHOD_firePageBack41] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("firePageBack", new Class[] {com.jm.commons.components.editor.HtmlPageEvent.class})); // NOI18N
            methods[METHOD_firePageBack41].setDisplayName ( "" );
            methods[METHOD_firePageForward42] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("firePageForward", new Class[] {com.jm.commons.components.editor.HtmlPageEvent.class})); // NOI18N
            methods[METHOD_firePageForward42].setDisplayName ( "" );
            methods[METHOD_firePageOpened43] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("firePageOpened", new Class[] {com.jm.commons.components.editor.HtmlPageEvent.class})); // NOI18N
            methods[METHOD_firePageOpened43].setDisplayName ( "" );
            methods[METHOD_firePageOpening44] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("firePageOpening", new Class[] {com.jm.commons.components.editor.HtmlPageEvent.class})); // NOI18N
            methods[METHOD_firePageOpening44].setDisplayName ( "" );
            methods[METHOD_firePropertyChange45] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, byte.class, byte.class})); // NOI18N
            methods[METHOD_firePropertyChange45].setDisplayName ( "" );
            methods[METHOD_firePropertyChange46] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, short.class, short.class})); // NOI18N
            methods[METHOD_firePropertyChange46].setDisplayName ( "" );
            methods[METHOD_firePropertyChange47] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, long.class, long.class})); // NOI18N
            methods[METHOD_firePropertyChange47].setDisplayName ( "" );
            methods[METHOD_firePropertyChange48] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, float.class, float.class})); // NOI18N
            methods[METHOD_firePropertyChange48].setDisplayName ( "" );
            methods[METHOD_firePropertyChange49] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, double.class, double.class})); // NOI18N
            methods[METHOD_firePropertyChange49].setDisplayName ( "" );
            methods[METHOD_firePropertyChange50] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, boolean.class, boolean.class})); // NOI18N
            methods[METHOD_firePropertyChange50].setDisplayName ( "" );
            methods[METHOD_firePropertyChange51] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, int.class, int.class})); // NOI18N
            methods[METHOD_firePropertyChange51].setDisplayName ( "" );
            methods[METHOD_firePropertyChange52] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, char.class, char.class})); // NOI18N
            methods[METHOD_firePropertyChange52].setDisplayName ( "" );
            methods[METHOD_forward53] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("forward", new Class[] {})); // NOI18N
            methods[METHOD_forward53].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke54] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getActionForKeyStroke54].setDisplayName ( "" );
            methods[METHOD_getBaseline55] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getBaseline", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_getBaseline55].setDisplayName ( "" );
            methods[METHOD_getBounds56] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_getBounds56].setDisplayName ( "" );
            methods[METHOD_getClientProperty57] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class})); // NOI18N
            methods[METHOD_getClientProperty57].setDisplayName ( "" );
            methods[METHOD_getComponentAt58] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentAt", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_getComponentAt58].setDisplayName ( "" );
            methods[METHOD_getComponentAt59] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getComponentAt59].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder60] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_getComponentZOrder60].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke61] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getConditionForKeyStroke61].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale62] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getDefaultLocale", new Class[] {})); // NOI18N
            methods[METHOD_getDefaultLocale62].setDisplayName ( "" );
            methods[METHOD_getEditorKitClassNameForContentType63] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("getEditorKitClassNameForContentType", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getEditorKitClassNameForContentType63].setDisplayName ( "" );
            methods[METHOD_getEditorKitForContentType64] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("getEditorKitForContentType", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getEditorKitForContentType64].setDisplayName ( "" );
            methods[METHOD_getFocusTraversalKeys65] = new MethodDescriptor(java.awt.Container.class.getMethod("getFocusTraversalKeys", new Class[] {int.class})); // NOI18N
            methods[METHOD_getFocusTraversalKeys65].setDisplayName ( "" );
            methods[METHOD_getFontMetrics66] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class})); // NOI18N
            methods[METHOD_getFontMetrics66].setDisplayName ( "" );
            methods[METHOD_getInsets67] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getInsets", new Class[] {java.awt.Insets.class})); // NOI18N
            methods[METHOD_getInsets67].setDisplayName ( "" );
            methods[METHOD_getKeymap68] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getKeymap", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getKeymap68].setDisplayName ( "" );
            methods[METHOD_getListeners69] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getListeners", new Class[] {java.lang.Class.class})); // NOI18N
            methods[METHOD_getListeners69].setDisplayName ( "" );
            methods[METHOD_getLocation70] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getLocation", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getLocation70].setDisplayName ( "" );
            methods[METHOD_getMousePosition71] = new MethodDescriptor(java.awt.Container.class.getMethod("getMousePosition", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_getMousePosition71].setDisplayName ( "" );
            methods[METHOD_getPage72] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("getPage", new Class[] {})); // NOI18N
            methods[METHOD_getPage72].setDisplayName ( "" );
            methods[METHOD_getPopupLocation73] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getPopupLocation73].setDisplayName ( "" );
            methods[METHOD_getPrintable74] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getPrintable", new Class[] {java.text.MessageFormat.class, java.text.MessageFormat.class})); // NOI18N
            methods[METHOD_getPrintable74].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners75] = new MethodDescriptor(java.awt.Component.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getPropertyChangeListeners75].setDisplayName ( "" );
            methods[METHOD_getScrollableBlockIncrement76] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getScrollableBlockIncrement", new Class[] {java.awt.Rectangle.class, int.class, int.class})); // NOI18N
            methods[METHOD_getScrollableBlockIncrement76].setDisplayName ( "" );
            methods[METHOD_getScrollableUnitIncrement77] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getScrollableUnitIncrement", new Class[] {java.awt.Rectangle.class, int.class, int.class})); // NOI18N
            methods[METHOD_getScrollableUnitIncrement77].setDisplayName ( "" );
            methods[METHOD_getSize78] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getSize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_getSize78].setDisplayName ( "" );
            methods[METHOD_getText79] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getText", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_getText79].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation80] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipLocation80].setDisplayName ( "" );
            methods[METHOD_getToolTipText81] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipText81].setDisplayName ( "" );
            methods[METHOD_gotFocus82] = new MethodDescriptor(java.awt.Component.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_gotFocus82].setDisplayName ( "" );
            methods[METHOD_grabFocus83] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("grabFocus", new Class[] {})); // NOI18N
            methods[METHOD_grabFocus83].setDisplayName ( "" );
            methods[METHOD_handleEvent84] = new MethodDescriptor(java.awt.Component.class.getMethod("handleEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_handleEvent84].setDisplayName ( "" );
            methods[METHOD_hasFocus85] = new MethodDescriptor(java.awt.Component.class.getMethod("hasFocus", new Class[] {})); // NOI18N
            methods[METHOD_hasFocus85].setDisplayName ( "" );
            methods[METHOD_hide86] = new MethodDescriptor(java.awt.Component.class.getMethod("hide", new Class[] {})); // NOI18N
            methods[METHOD_hide86].setDisplayName ( "" );
            methods[METHOD_imageUpdate87] = new MethodDescriptor(java.awt.Component.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, int.class, int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_imageUpdate87].setDisplayName ( "" );
            methods[METHOD_insets88] = new MethodDescriptor(java.awt.Container.class.getMethod("insets", new Class[] {})); // NOI18N
            methods[METHOD_insets88].setDisplayName ( "" );
            methods[METHOD_inside89] = new MethodDescriptor(java.awt.Component.class.getMethod("inside", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_inside89].setDisplayName ( "" );
            methods[METHOD_invalidate90] = new MethodDescriptor(java.awt.Container.class.getMethod("invalidate", new Class[] {})); // NOI18N
            methods[METHOD_invalidate90].setDisplayName ( "" );
            methods[METHOD_isAncestorOf91] = new MethodDescriptor(java.awt.Container.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isAncestorOf91].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot92] = new MethodDescriptor(java.awt.Container.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class})); // NOI18N
            methods[METHOD_isFocusCycleRoot92].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent93] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isLightweightComponent93].setDisplayName ( "" );
            methods[METHOD_keyDown94] = new MethodDescriptor(java.awt.Component.class.getMethod("keyDown", new Class[] {java.awt.Event.class, int.class})); // NOI18N
            methods[METHOD_keyDown94].setDisplayName ( "" );
            methods[METHOD_keyUp95] = new MethodDescriptor(java.awt.Component.class.getMethod("keyUp", new Class[] {java.awt.Event.class, int.class})); // NOI18N
            methods[METHOD_keyUp95].setDisplayName ( "" );
            methods[METHOD_layout96] = new MethodDescriptor(java.awt.Container.class.getMethod("layout", new Class[] {})); // NOI18N
            methods[METHOD_layout96].setDisplayName ( "" );
            methods[METHOD_list97] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] {})); // NOI18N
            methods[METHOD_list97].setDisplayName ( "" );
            methods[METHOD_list98] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] {java.io.PrintStream.class})); // NOI18N
            methods[METHOD_list98].setDisplayName ( "" );
            methods[METHOD_list99] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] {java.io.PrintWriter.class})); // NOI18N
            methods[METHOD_list99].setDisplayName ( "" );
            methods[METHOD_list100] = new MethodDescriptor(java.awt.Container.class.getMethod("list", new Class[] {java.io.PrintStream.class, int.class})); // NOI18N
            methods[METHOD_list100].setDisplayName ( "" );
            methods[METHOD_list101] = new MethodDescriptor(java.awt.Container.class.getMethod("list", new Class[] {java.io.PrintWriter.class, int.class})); // NOI18N
            methods[METHOD_list101].setDisplayName ( "" );
            methods[METHOD_loadKeymap102] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("loadKeymap", new Class[] {javax.swing.text.Keymap.class, javax.swing.text.JTextComponent.KeyBinding[].class, javax.swing.Action[].class})); // NOI18N
            methods[METHOD_loadKeymap102].setDisplayName ( "" );
            methods[METHOD_locate103] = new MethodDescriptor(java.awt.Container.class.getMethod("locate", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_locate103].setDisplayName ( "" );
            methods[METHOD_location104] = new MethodDescriptor(java.awt.Component.class.getMethod("location", new Class[] {})); // NOI18N
            methods[METHOD_location104].setDisplayName ( "" );
            methods[METHOD_lostFocus105] = new MethodDescriptor(java.awt.Component.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_lostFocus105].setDisplayName ( "" );
            methods[METHOD_minimumSize106] = new MethodDescriptor(java.awt.Container.class.getMethod("minimumSize", new Class[] {})); // NOI18N
            methods[METHOD_minimumSize106].setDisplayName ( "" );
            methods[METHOD_modelToView107] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("modelToView", new Class[] {int.class})); // NOI18N
            methods[METHOD_modelToView107].setDisplayName ( "" );
            methods[METHOD_mouseDown108] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseDown108].setDisplayName ( "" );
            methods[METHOD_mouseDrag109] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseDrag109].setDisplayName ( "" );
            methods[METHOD_mouseEnter110] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseEnter110].setDisplayName ( "" );
            methods[METHOD_mouseExit111] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseExit111].setDisplayName ( "" );
            methods[METHOD_mouseMove112] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseMove112].setDisplayName ( "" );
            methods[METHOD_mouseUp113] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseUp113].setDisplayName ( "" );
            methods[METHOD_move114] = new MethodDescriptor(java.awt.Component.class.getMethod("move", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_move114].setDisplayName ( "" );
            methods[METHOD_moveCaretPosition115] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("moveCaretPosition", new Class[] {int.class})); // NOI18N
            methods[METHOD_moveCaretPosition115].setDisplayName ( "" );
            methods[METHOD_nextFocus116] = new MethodDescriptor(java.awt.Component.class.getMethod("nextFocus", new Class[] {})); // NOI18N
            methods[METHOD_nextFocus116].setDisplayName ( "" );
            methods[METHOD_paint117] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paint", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paint117].setDisplayName ( "" );
            methods[METHOD_paintAll118] = new MethodDescriptor(java.awt.Component.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintAll118].setDisplayName ( "" );
            methods[METHOD_paintComponents119] = new MethodDescriptor(java.awt.Container.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintComponents119].setDisplayName ( "" );
            methods[METHOD_paintImmediately120] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paintImmediately", new Class[] {int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_paintImmediately120].setDisplayName ( "" );
            methods[METHOD_paintImmediately121] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_paintImmediately121].setDisplayName ( "" );
            methods[METHOD_paste122] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("paste", new Class[] {})); // NOI18N
            methods[METHOD_paste122].setDisplayName ( "" );
            methods[METHOD_postEvent123] = new MethodDescriptor(java.awt.Component.class.getMethod("postEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_postEvent123].setDisplayName ( "" );
            methods[METHOD_preferredSize124] = new MethodDescriptor(java.awt.Container.class.getMethod("preferredSize", new Class[] {})); // NOI18N
            methods[METHOD_preferredSize124].setDisplayName ( "" );
            methods[METHOD_prepareImage125] = new MethodDescriptor(java.awt.Component.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage125].setDisplayName ( "" );
            methods[METHOD_prepareImage126] = new MethodDescriptor(java.awt.Component.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, int.class, int.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage126].setDisplayName ( "" );
            methods[METHOD_print127] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("print", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_print127].setDisplayName ( "" );
            methods[METHOD_print128] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("print", new Class[] {})); // NOI18N
            methods[METHOD_print128].setDisplayName ( "" );
            methods[METHOD_print129] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("print", new Class[] {java.text.MessageFormat.class, java.text.MessageFormat.class})); // NOI18N
            methods[METHOD_print129].setDisplayName ( "" );
            methods[METHOD_print130] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("print", new Class[] {java.text.MessageFormat.class, java.text.MessageFormat.class, boolean.class, javax.print.PrintService.class, javax.print.attribute.PrintRequestAttributeSet.class, boolean.class})); // NOI18N
            methods[METHOD_print130].setDisplayName ( "" );
            methods[METHOD_printAll131] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("printAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printAll131].setDisplayName ( "" );
            methods[METHOD_printComponents132] = new MethodDescriptor(java.awt.Container.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printComponents132].setDisplayName ( "" );
            methods[METHOD_putClientProperty133] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_putClientProperty133].setDisplayName ( "" );
            methods[METHOD_read134] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("read", new Class[] {java.io.Reader.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_read134].setDisplayName ( "" );
            methods[METHOD_read135] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("read", new Class[] {java.io.InputStream.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_read135].setDisplayName ( "" );
            methods[METHOD_registerEditorKitForContentType136] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("registerEditorKitForContentType", new Class[] {java.lang.String.class, java.lang.String.class})); // NOI18N
            methods[METHOD_registerEditorKitForContentType136].setDisplayName ( "" );
            methods[METHOD_registerEditorKitForContentType137] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("registerEditorKitForContentType", new Class[] {java.lang.String.class, java.lang.String.class, java.lang.ClassLoader.class})); // NOI18N
            methods[METHOD_registerEditorKitForContentType137].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction138] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, int.class})); // NOI18N
            methods[METHOD_registerKeyboardAction138].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction139] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, int.class})); // NOI18N
            methods[METHOD_registerKeyboardAction139].setDisplayName ( "" );
            methods[METHOD_remove140] = new MethodDescriptor(java.awt.Component.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class})); // NOI18N
            methods[METHOD_remove140].setDisplayName ( "" );
            methods[METHOD_remove141] = new MethodDescriptor(java.awt.Container.class.getMethod("remove", new Class[] {int.class})); // NOI18N
            methods[METHOD_remove141].setDisplayName ( "" );
            methods[METHOD_remove142] = new MethodDescriptor(java.awt.Container.class.getMethod("remove", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_remove142].setDisplayName ( "" );
            methods[METHOD_removeAll143] = new MethodDescriptor(java.awt.Container.class.getMethod("removeAll", new Class[] {})); // NOI18N
            methods[METHOD_removeAll143].setDisplayName ( "" );
            methods[METHOD_removeHtmlViewerListner144] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("removeHtmlViewerListner", new Class[] {com.jm.commons.components.editor.HtmlPageListener.class})); // NOI18N
            methods[METHOD_removeHtmlViewerListner144].setDisplayName ( "" );
            methods[METHOD_removeKeymap145] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("removeKeymap", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_removeKeymap145].setDisplayName ( "" );
            methods[METHOD_removeNotify146] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("removeNotify", new Class[] {})); // NOI18N
            methods[METHOD_removeNotify146].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener147] = new MethodDescriptor(java.awt.Component.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_removePropertyChangeListener147].setDisplayName ( "" );
            methods[METHOD_repaint148] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] {})); // NOI18N
            methods[METHOD_repaint148].setDisplayName ( "" );
            methods[METHOD_repaint149] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] {long.class})); // NOI18N
            methods[METHOD_repaint149].setDisplayName ( "" );
            methods[METHOD_repaint150] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] {int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_repaint150].setDisplayName ( "" );
            methods[METHOD_repaint151] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("repaint", new Class[] {long.class, int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_repaint151].setDisplayName ( "" );
            methods[METHOD_repaint152] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_repaint152].setDisplayName ( "" );
            methods[METHOD_replaceSelection153] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("replaceSelection", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_replaceSelection153].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus154] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestDefaultFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestDefaultFocus154].setDisplayName ( "" );
            methods[METHOD_requestFocus155] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestFocus155].setDisplayName ( "" );
            methods[METHOD_requestFocus156] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocus", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_requestFocus156].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow157] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocusInWindow", new Class[] {})); // NOI18N
            methods[METHOD_requestFocusInWindow157].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions158] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("resetKeyboardActions", new Class[] {})); // NOI18N
            methods[METHOD_resetKeyboardActions158].setDisplayName ( "" );
            methods[METHOD_reshape159] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("reshape", new Class[] {int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_reshape159].setDisplayName ( "" );
            methods[METHOD_resize160] = new MethodDescriptor(java.awt.Component.class.getMethod("resize", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_resize160].setDisplayName ( "" );
            methods[METHOD_resize161] = new MethodDescriptor(java.awt.Component.class.getMethod("resize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_resize161].setDisplayName ( "" );
            methods[METHOD_revalidate162] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("revalidate", new Class[] {})); // NOI18N
            methods[METHOD_revalidate162].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible163] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_scrollRectToVisible163].setDisplayName ( "" );
            methods[METHOD_scrollToReference164] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("scrollToReference", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_scrollToReference164].setDisplayName ( "" );
            methods[METHOD_select165] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("select", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_select165].setDisplayName ( "" );
            methods[METHOD_selectAll166] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("selectAll", new Class[] {})); // NOI18N
            methods[METHOD_selectAll166].setDisplayName ( "" );
            methods[METHOD_setBounds167] = new MethodDescriptor(java.awt.Component.class.getMethod("setBounds", new Class[] {int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_setBounds167].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder168] = new MethodDescriptor(java.awt.Container.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, int.class})); // NOI18N
            methods[METHOD_setComponentZOrder168].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale169] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class})); // NOI18N
            methods[METHOD_setDefaultLocale169].setDisplayName ( "" );
            methods[METHOD_setEditorKitForContentType170] = new MethodDescriptor(javax.swing.JEditorPane.class.getMethod("setEditorKitForContentType", new Class[] {java.lang.String.class, javax.swing.text.EditorKit.class})); // NOI18N
            methods[METHOD_setEditorKitForContentType170].setDisplayName ( "" );
            methods[METHOD_setPage171] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("setPage", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_setPage171].setDisplayName ( "" );
            methods[METHOD_setPage172] = new MethodDescriptor(JMEditorPaneBeanInfo.class.getMethod("setPage", new Class[] {java.net.URL.class})); // NOI18N
            methods[METHOD_setPage172].setDisplayName ( "" );
            methods[METHOD_show173] = new MethodDescriptor(java.awt.Component.class.getMethod("show", new Class[] {})); // NOI18N
            methods[METHOD_show173].setDisplayName ( "" );
            methods[METHOD_show174] = new MethodDescriptor(java.awt.Component.class.getMethod("show", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_show174].setDisplayName ( "" );
            methods[METHOD_size175] = new MethodDescriptor(java.awt.Component.class.getMethod("size", new Class[] {})); // NOI18N
            methods[METHOD_size175].setDisplayName ( "" );
            methods[METHOD_toString176] = new MethodDescriptor(java.awt.Component.class.getMethod("toString", new Class[] {})); // NOI18N
            methods[METHOD_toString176].setDisplayName ( "" );
            methods[METHOD_transferFocus177] = new MethodDescriptor(java.awt.Component.class.getMethod("transferFocus", new Class[] {})); // NOI18N
            methods[METHOD_transferFocus177].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward178] = new MethodDescriptor(java.awt.Component.class.getMethod("transferFocusBackward", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusBackward178].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle179] = new MethodDescriptor(java.awt.Container.class.getMethod("transferFocusDownCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusDownCycle179].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle180] = new MethodDescriptor(java.awt.Component.class.getMethod("transferFocusUpCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusUpCycle180].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction181] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_unregisterKeyboardAction181].setDisplayName ( "" );
            methods[METHOD_update182] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("update", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_update182].setDisplayName ( "" );
            methods[METHOD_updateUI183] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("updateUI", new Class[] {})); // NOI18N
            methods[METHOD_updateUI183].setDisplayName ( "" );
            methods[METHOD_validate184] = new MethodDescriptor(java.awt.Container.class.getMethod("validate", new Class[] {})); // NOI18N
            methods[METHOD_validate184].setDisplayName ( "" );
            methods[METHOD_viewToModel185] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("viewToModel", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_viewToModel185].setDisplayName ( "" );
            methods[METHOD_write186] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("write", new Class[] {java.io.Writer.class})); // NOI18N
            methods[METHOD_write186].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods

        // Here you can add code for customizing the methods array.
        
        return methods;     }//GEN-LAST:Methods
    private static java.awt.Image iconColor16 = null;//GEN-BEGIN:IconsDef
    private static java.awt.Image iconColor32 = null;
    private static java.awt.Image iconMono16 = null;
    private static java.awt.Image iconMono32 = null;//GEN-END:IconsDef
    private static String iconNameC16 = "/com/jm/commons/icons/jm_logo_16x16.png";//GEN-BEGIN:Icons
    private static String iconNameC32 = "/com/jm/commons/icons/jm_logo_32x32.png";
    private static String iconNameM16 = "/com/jm/commons/icons/jm_logo_mono_16x16.png";
    private static String iconNameM32 = "/com/jm/commons/icons/jm_logo_mono_32x32.png";//GEN-END:Icons
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx

    public BeanInfo[] getAdditionalBeanInfo() {//GEN-FIRST:Superclass
        Class superclass = JMEditorPaneBeanInfo.class.getSuperclass();
        BeanInfo sbi = null;
        try {
            sbi = Introspector.getBeanInfo(superclass);//GEN-HEADEREND:Superclass
    // Here you can add code for customizing the Superclass BeanInfo.
            } catch(IntrospectionException ex) { }  return new BeanInfo[] { sbi }; }//GEN-LAST:Superclass
    /**
     * Gets the bean's
     * <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable properties of this bean.
     * May return null if the information should be obtained by automatic
     * analysis.
     */
    @Override
    public BeanDescriptor getBeanDescriptor() {
        return getBdescriptor();
    }

    /**
     * Gets the bean's
     * <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean. May return null if the information
     * should be obtained by automatic analysis. <p> If a property is indexed,
     * then its entry in the result array will belong to the
     * IndexedPropertyDescriptor subclass of PropertyDescriptor. A client of
     * getPropertyDescriptors can use "instanceof" to check if a given
     * PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        return getPdescriptor();
    }

    /**
     * Gets the bean's
     * <code>EventSetDescriptor</code>s.
     *
     * @return An array of EventSetDescriptors describing the kinds of events
     * fired by this bean. May return null if the information should be obtained
     * by automatic analysis.
     */
    @Override
    public EventSetDescriptor[] getEventSetDescriptors() {
        return getEdescriptor();
    }

    /**
     * Gets the bean's
     * <code>MethodDescriptor</code>s.
     *
     * @return An array of MethodDescriptors describing the methods implemented
     * by this bean. May return null if the information should be obtained by
     * automatic analysis.
     */
    @Override
    public MethodDescriptor[] getMethodDescriptors() {
        return getMdescriptor();
    }

    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     *
     * @return Index of default property in the PropertyDescriptor array
     * returned by getPropertyDescriptors. <P>	Returns -1 if there is no default
     * property.
     */
    @Override
    public int getDefaultPropertyIndex() {
        return defaultPropertyIndex;
    }

    /**
     * A bean may have a "default" event that is the event that will mostly
     * commonly be used by human's when using the bean.
     *
     * @return Index of default event in the EventSetDescriptor array returned
     * by getEventSetDescriptors. <P>	Returns -1 if there is no default event.
     */
    @Override
    public int getDefaultEventIndex() {
        return defaultEventIndex;
    }

    /**
     * This method returns an image object that can be used to represent the
     * bean in toolboxes, toolbars, etc. Icon images will typically be GIFs, but
     * may in future include other formats. <p> Beans aren't required to provide
     * icons and may return null from this method. <p> There are four possible
     * flavors of icons (16x16 color, 32x32 color, 16x16 mono, 32x32 mono). If a
     * bean choses to only support a single icon we recommend supporting 16x16
     * color. <p> We recommend that icons have a "transparent" background so
     * they can be rendered onto an existing background.
     *
     * @param iconKind The kind of icon requested. This should be one of the
     * constant values ICON_COLOR_16x16, ICON_COLOR_32x32, ICON_MONO_16x16, or
     * ICON_MONO_32x32.
     * @return An image object representing the requested icon. May return null
     * if no suitable icon is available.
     */
    @Override
    public java.awt.Image getIcon(int iconKind) {
        switch (iconKind) {
            case ICON_COLOR_16x16:
                if (iconNameC16 == null) {
                    return null;
                } else {
                    if (iconColor16 == null) {
                        iconColor16 = loadImage(iconNameC16);
                    }
                    return iconColor16;
                }
            case ICON_COLOR_32x32:
                if (iconNameC32 == null) {
                    return null;
                } else {
                    if (iconColor32 == null) {
                        iconColor32 = loadImage(iconNameC32);
                    }
                    return iconColor32;
                }
            case ICON_MONO_16x16:
                if (iconNameM16 == null) {
                    return null;
                } else {
                    if (iconMono16 == null) {
                        iconMono16 = loadImage(iconNameM16);
                    }
                    return iconMono16;
                }
            case ICON_MONO_32x32:
                if (iconNameM32 == null) {
                    return null;
                } else {
                    if (iconMono32 == null) {
                        iconMono32 = loadImage(iconNameM32);
                    }
                    return iconMono32;
                }
            default:
                return null;
        }
    }
}
