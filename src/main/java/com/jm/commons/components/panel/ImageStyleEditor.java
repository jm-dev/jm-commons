/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.panel;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The JavaBean for image style selection on the @code{ImagePanel}
 * 
 * @created Nov 27, 2012
 * @author Michael L.R. Marques
 */
public class ImageStyleEditor implements PropertyEditor {
    
    /**
     * 
     */
    private int value;
    
    /**
     * 
     */
    private List<PropertyChangeListener> listeners;
    
    /**
     * The image styles as strings
     */
    private static final String[] styles = new String[] { "None", "Stretch", "Center", "Custom" };
    
    /**
     * 
     */
    public ImageStyleEditor() {
        this.value = 0;
        this.listeners = new ArrayList();
    }
    
    /**
     * 
     * @return 
     */
    @Override public String getJavaInitializationString() {
        return this.getClass().getPackage().getName() + ".ImagePanel." + styles[this.value].toUpperCase();
    }
    
    /**
     * 
     * @return 
     */
    @Override public String getAsText() {
        return styles[this.value];
    }
    
    /**
     * 
     * @param text
     * @throws IllegalArgumentException 
     */
    @Override public void setAsText(String text) {
        setValue(Arrays.asList(styles).indexOf(text));
    }
    
    /**
     * 
     * @param value 
     */
    @Override public void setValue(Object value) {
        firePropertyChanged(this.value, (int) value);
        this.value = (int) value;
    }
    
    /**
     * 
     * @return 
     */
    @Override public Object getValue() {
        return this.value;
    }
    
    /**
     * 
     * @return 
     */
    @Override public String[] getTags() {
        return styles;
    }
    
    /**
     * 
     * @return 
     */
    @Override public boolean isPaintable() {
        return false;
    }
    
    /**
     * 
     * @param gfx
     * @param box 
     */
    @Override public void paintValue(Graphics gfx, Rectangle box) {}
    
    /**
     * 
     * @return 
     */
    @Override public Component getCustomEditor() {
        return null;
    }
    
    /**
     * 
     * @return 
     */
    @Override public boolean supportsCustomEditor() {
        return false;
    }
    
    /**
     * 
     * @param listener 
     */
    @Override public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.listeners.add(listener);
    }
    
    /**
     * 
     * @param listener 
     */
    @Override public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.listeners.remove(listener);
    }

    
    /**
     * 
     * @param oldValue
     * @param newValue 
     */
    public void firePropertyChanged(int oldValue, int newValue) {
        for (PropertyChangeListener listener : this.listeners) {
            listener.propertyChange(new PropertyChangeEvent(this, "imageStyle", oldValue, newValue));
        }
    }
    
}
