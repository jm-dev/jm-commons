/*
 * Copyright (C) 2013 Michael
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.jm.commons.components.combobox;

import java.awt.Color;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @created Feb 25, 2013
 * @author Michael L.R. Marques
 */
public class HintJComboBox extends JComboBox implements Serializable, PropertyChangeListener {
    
    /**
     * 
     */
    public static final String PROP_HINT_TEXT_PROPERTY = "hintText";
    
    /**
     * 
     */
    private String hintText;
    
    /**
     * 
     */
    private Font font;
    
    /**
     * 
     */
    public HintJComboBox() {
        super();
        setEditable(true);
    }
    
    /**
     * 
     */
    public HintJComboBox(String hintText) {
        this();
        this.hintText = hintText;
        super.updateUI();
    }
    
    /**
     * 
     * @param model 
     */
    public HintJComboBox(ComboBoxModel model) {
        super(model);
        setEditable(true);
    }
    
    /**
     * 
     * @param items 
     */
    public HintJComboBox(Object[] items) {
        super(items);
        setEditable(true);
    }
    
    /**
     * 
     * @param items 
     */
    public HintJComboBox(Vector items) {
        super(items);
        setEditable(true);
    }
    
    /**
     *
     * @return
     */
    public String getHintText() {
        return this.hintText;
    }

    /**
     *
     * @param hintText
     */
    public void setHintText(String hintText) {
        super.firePropertyChange(PROP_HINT_TEXT_PROPERTY, this.hintText, hintText);
        this.hintText = hintText;
        super.updateUI();
    }
    
    /**
     * 
     * @return 
     */
    @Override public Object getSelectedItem() {
        if (super.getSelectedIndex() == -1 ||
                super.getSelectedItem() == null) {
            super.setForeground(Color.gray);
            super.setFont(super.getFont().deriveFont(Font.ITALIC));
            return this.hintText;
        }
        super.setFont(this.font);
        return super.getSelectedItem();
    }
    
    /**
     * 
     * @param editable 
     */
    @Override public void setEditable(boolean editable) {
        super.setEditable(true);
    }
    
    /**
     * 
     * @param evt 
     */
    @Override public void propertyChange(PropertyChangeEvent evt) {
        //
        if (evt.getPropertyName().equals("font.style") &&
                !evt.getNewValue().equals(this.font)) {
            this.font = (Font) evt.getNewValue();
        }
    }
    
}
