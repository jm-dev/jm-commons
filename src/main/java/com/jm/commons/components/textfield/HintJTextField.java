/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.textfield;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.Serializable;
import javax.swing.JTextField;
import javax.swing.text.Document;

/**
 *
 * @author Michael L.R. Marques
 */
public class HintJTextField extends JTextField implements FocusListener, Serializable {
    
    /**
     * 
     */
    public static final String PROP_HINT_TEXT_PROPERTY = "hintText";
    
    /**
     * 
     */
    private String hintText = "";
    
    /**
     * 
     */
    private Color color;
    
    /**
     * 
     */
    private int fontStyle;

    /**
     * 
     */
    public HintJTextField() {
        super();
        super.addFocusListener(this);
    }
    
    /**
     *
     * @param hintText
     */
    public HintJTextField(final String hintText) {
        super(null, hintText, 0);
        this.hintText = hintText;
        super.addFocusListener(this);
        this.focusLost(null);
        super.updateUI();
    }
    
    /**
     * 
     * @param columns 
     */
    public HintJTextField(int columns) {
        super(null, null, columns);
        super.addFocusListener(this);
    }
    
    /**
     * 
     * @param text
     * @param columns 
     */
    public HintJTextField(String hintText, int columns) {
        super(null, hintText, columns);
        this.hintText = hintText;
        super.addFocusListener(this);
        this.focusLost(null);
        super.updateUI();
    }
    
    /**
     * 
     * @param doc
     * @param hintText
     * @param columns 
     */
    public HintJTextField(Document doc, String hintText, int columns) {
        super(doc, hintText, columns);
        this.hintText = hintText;
        super.addFocusListener(this);
        this.focusLost(null);
        super.updateUI();
    }

    /**
     *
     * @param fe
     */
    @Override public void focusGained(FocusEvent fe) {
        if (this.getText().isEmpty()) {
            super.setText("");
            super.setForeground(this.color);
            super.setFont(super.getFont().deriveFont(this.fontStyle));
        }
    }

    /**
     *
     * @param fe
     */
    @Override public void focusLost(FocusEvent fe) {
        if (this.getText().isEmpty()) {
            this.color = super.getForeground();
            this.fontStyle = super.getFont().getStyle();
            super.setFont(super.getFont().deriveFont(Font.ITALIC));
            super.setForeground(Color.gray);
            super.setText(this.hintText);
        }
    }

    /**
     *
     * @return
     */
    @Override public String getText() {
        return super.getText().equals(this.hintText) ? "" : super.getText();
    }
    
    /**
     * 
     * @param text 
     */
    @Override public void setText(String text) {
        super.setText(text);
        
        if (!this.hintText.equals(text)) {
            super.setFont(super.getFont().deriveFont(Font.PLAIN));
            super.setForeground(Color.black);
        }
    }

    /**
     *
     * @return
     */
    public String getHintText() {
        return this.hintText;
    }

    /**
     *
     * @param hintText
     */
    public void setHintText(String hintText) {
        super.firePropertyChange(PROP_HINT_TEXT_PROPERTY, this.hintText, hintText);
        this.hintText = hintText;
        this.focusLost(null);
        super.updateUI();
    }
    
}
