/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jm.commons.components;

import com.jm.commons.ui.UIBounds;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JDialog;

/**
 *
 * @author Marquema
 */
public class JMDialog extends JDialog implements ComponentListener {
    
    /**
     * 
     */
    public JMDialog() {
        super();
        addComponentListener(this);
    }
    
    /**
     * 
     * @param owner 
     */
    public JMDialog(Frame owner) {
        super(owner);
        addComponentListener(this);
        setBounds(UIBounds.getBounds(owner, this));
    }
    
    /**
     * 
     * @param owner
     * @param modal 
     */
    public JMDialog(Frame owner, boolean modal) {
        super(owner, modal);
        addComponentListener(this);
        setBounds(UIBounds.getBounds(owner, this));
    }
    
    /**
     * 
     * @param owner
     */
    public void setBoundsToCenterOfParent(Container owner) {
        setBounds(UIBounds.getBounds(owner, this));
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void componentResized(ComponentEvent e) {
        Dimension maxSize = getMaximumSize();
        Dimension minSize = getMinimumSize();
        
        int width = (int) Math.ceil(getSize().getWidth());
        if (maxSize.getWidth() > 0 &&
                width > maxSize.getWidth()) {
            width = (int) Math.ceil(maxSize.getWidth());
        } else if (minSize.getWidth() > 0 &&
                width < minSize.getWidth()) {
            width = (int) Math.ceil(minSize.getWidth());
        }
        
        int height = (int) Math.ceil(getSize().getHeight());
        if (maxSize.getHeight() > 0 &&
                height > maxSize.getHeight()) {
            height = (int) Math.ceil(maxSize.getHeight());
        } else if (minSize.getHeight() > 0 &&
                height < minSize.getHeight()) {
            height = (int) Math.ceil(minSize.getHeight());
        }
        
        setSize(width, height);
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void componentMoved(ComponentEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void componentShown(ComponentEvent e) {
        
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void componentHidden(ComponentEvent e) {
        
    }
    
}
