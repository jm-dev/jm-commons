/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.help.models;

import com.jm.commons.components.help.objects.HelpFile;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataListener;

/**
 * Renders a collection of hyper-text help files in list format
 * From search criteria
 * 
 * @created Nov 30, 2012
 * @author Michael
 */
public class HelpSearchListModel implements ListModel {

    /**
     * Help file collection
     */
    private List<HelpFile> files;
    
    /**
     * Event listener collection
     */
    protected EventListenerList listeners;
    
    /**
     * A complete list of searched help files
     * 
     * @param directory 
     */
    public HelpSearchListModel(HelpFile directory, String search, boolean caseSensitive, boolean regex, boolean insideDocument) {
        this.listeners = new EventListenerList();
        this.files = directory.listFiles(true, false, search, caseSensitive, regex);
        if (insideDocument) {
            for (HelpFile file : directory.listFiles(true, false)) {
                try {
                    try (BufferedReader reader = new BufferedReader(new FileReader(file.getFile()))) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                            if ((search == null || search.isEmpty()) ||
                                    (regex && line.matches(search))  ||
                                        (caseSensitive ? line.contains(search) : line.toUpperCase().contains(search.toUpperCase()))) {
                                if (!this.files.contains(file)) {
                                    this.files.add(file);
                                }
                                break;
                            }
                        }
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }
    
    /**
     * The size of the collection of help files
     * 
     * @return 
     */
    @Override public int getSize() {
        return this.files.size();
    }
    
    /**
     * The help file at index
     * 
     * @param index
     * @return 
     */
    @Override public Object getElementAt(int index) {
        return this.files.get(index);
    }
    
    /**
     * Adds a @code{ListDataListener} to the event listener collection
     * 
     * @param l 
     */
    @Override public void addListDataListener(ListDataListener l) {
        this.listeners.add(ListDataListener.class, l);
    }
    
    /**
     * Removes a @code{ListDataListener} from the event listener collection
     * 
     * @param l 
     */
    @Override public void removeListDataListener(ListDataListener l) {
        this.listeners.remove(ListDataListener.class, l);
    }

}
