/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.help.models;

import com.jm.commons.components.help.objects.HelpFile;
import java.io.Serializable;
import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * A Tree Model that will render hyper-text help files
 * in a directory format
 * 
 * @created Nov 30, 2012
 * @author Michael L.R. Marques
 */
public class HelpTreeModel implements TreeModel, Serializable {
    
    /**
     * The help file root object
     */
    private HelpFile root;
    
    /**
     * The event listener collection
     */
    protected EventListenerList listeners;
    
    /**
     * Creates the {@code HelpTreeModel}
     * 
     * @param root
     */
    public HelpTreeModel(HelpFile root) {
        this.root = root;
        this.listeners = new EventListenerList();
    }
    
    /**
     * Returns the root help file
     * 
     * @return Object
     */
    @Override public Object getRoot() {
        return this.root;
    }
    
    /**
     * Returns the child of the parent
     * 
     * @param parent
     * @param index
     * @return Object
     */
    @Override public Object getChild(Object parent, int index) {
       return ((HelpFile) parent).listFiles(true).get(index);
    }
    
    /**
     * Gets the number of children from the parent
     * 
     * @param parent
     * @return int
     */
    @Override public int getChildCount(Object parent) {
        return ((HelpFile) parent).fileCount(true);
    }
    
    /**
     * Returns the boolean of whether the node is a leaf
     * 
     * @param node
     * @return boolean
     */
    @Override public boolean isLeaf(Object node) {
        return !((HelpFile) node).isDirectory();
    }
    
    /**
     * Not needed
     * 
     * @param path
     * @param newValue
     */
    @Override public void valueForPathChanged(TreePath path, Object newValue) {}
    
    /**
     * Returns the index of a child
     * 
     * @param parent
     * @param child
     * @return int
     */
    @Override public int getIndexOfChild(Object parent, Object child) {
        return ((HelpFile) parent).listFiles(true).indexOf(child);
    }
    
    /**
     * Adds a {@code TreeModelListener} to the event listener collection
     * 
     * @param l
     */
    @Override public void addTreeModelListener(TreeModelListener l) {
        this.listeners.add(TreeModelListener.class, l);
    }
    
    /**
     * Removes a {@code TreeModelListener} from the event listener collection
     * 
     * @param l
     */
    @Override public void removeTreeModelListener(TreeModelListener l) {
        this.listeners.remove(TreeModelListener.class, l);
    }

}
