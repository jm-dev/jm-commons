/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.help.renderers;

import com.jm.commons.components.help.objects.HelpFile;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @created Dec 3, 2012
 * @author Michael L.R. Marques
 */
public class HelpListCellRenderer extends JLabel implements ListCellRenderer {
    
    /**
     * 
     */
    private HelpListCellRenderer() {
        super();
    }
    
    /**
     * 
     * @return 
     */
    public static ListCellRenderer getInstance() {
        return new HelpListCellRenderer();
    }
    
    /**
     * 
     * @param list
     * @param value
     * @param index
     * @param isSelected
     * @param cellHasFocus
     * @return 
     */
    @Override public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel label = ((HelpFile) value).getRenderer(list, isSelected);
        label.setForeground(isSelected ? Color.gray : Color.black);
        label.setBackground(Color.white);
        label.setOpaque(true);
        return label;
    }

}
