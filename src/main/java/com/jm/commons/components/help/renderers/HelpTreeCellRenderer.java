/*
 * Copyright (C) 2012-2013 Michael L.R. Marques
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Contact: michaellrmarques@gmail.com
 */

package com.jm.commons.components.help.renderers;

import com.jm.commons.components.help.objects.HelpFile;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @created Dec 3, 2012
 * @author Michael L.R. Marques
 */
public class HelpTreeCellRenderer extends JLabel implements TreeCellRenderer {
    
    /**
     * 
     */
    private HelpTreeCellRenderer() {
        super();
    }
    
    /**
     * 
     * @return 
     */
    public static TreeCellRenderer getInstance() {
        return new HelpTreeCellRenderer();
    }
    
    /**
     * 
     * @param tree
     * @param value
     * @param selected
     * @param expanded
     * @param leaf
     * @param row
     * @param hasFocus
     * @return 
     */
    @Override public Component getTreeCellRendererComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        if (value instanceof HelpFile) {
            JLabel label = ((HelpFile) value).getRenderer(tree, isSelected);
            label.setForeground(isSelected ? Color.darkGray : Color.black);
            return label;
        }
        return new JLabel("root");
    }

}
